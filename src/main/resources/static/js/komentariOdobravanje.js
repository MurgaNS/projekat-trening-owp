$(document).ready(function() {
    function sviKomentari(){
        $.ajax({
            url: "/Trening/api/komentari/odobravanje",
            type: "get", //send it through get method
            success: function(response) {
                $("#komentari").empty();
                $.each(response, function (key, value) {
                    $('#komentari').append("<tr>\
										<td>"+value.tekst+"</td>\
										<td>"+"<button class='odbij' data-id=" + value.id + ">Odbij</button>"+"</td>\
										<td>"+"<button class='odobri' data-id=" + value.id + ">Odobri</button>"+"</td>\
										</tr>");
                })
            },
            error: function(xhr) {
                //Do Something to handle error
            }
        });
    }


    sviKomentari();
    $(document).on("click", ".odbij", function (evt) {
        evt.preventDefault();
        let idKomentara = $(this).data("id");

        $.ajax({
            url: "/Trening/api/komentari/" + idKomentara + "/odbij",
            type: "post", //send it through get method
            success: function(response) {
                sviKomentari();
            },
            error: function(xhr) {
                //Do Something to handle error
            }
        });

    });

    $(document).on("click", ".odobri", function (evt) {
        let idKomentara = $(this).data("id");
        evt.preventDefault();
        $.ajax({
            url: "/Trening/api/komentari/" + idKomentara +   "/odobri",
            type: "post", //send it through get method
            success: function(response) {
                sviKomentari();
            },
            error: function(xhr) {
                //Do Something to handle error
            }
        });

    });


    /*
    function popuniFilmove() {
        // čitanje parametara forme za pretragu
        var naziv = nazivInput.val()
        var zanrId = zanrIdSelect.find("option:selected").val()
        var trajanjeOd = trajanjeOdInput.val()
        var trajanjeDo = trajanjeDoInput.val()

        // parametri zahteva
        var params = {
            naziv: naziv,
            zanrId: zanrId,
            trajanjeOd: trajanjeOd,
            trajanjeDo: trajanjeDo
        }
        console.log(params)
        $.get(baseURL+"Filmovi/Search", params, function(odgovor) {
            console.log(odgovor)

            if (odgovor.status == "ok") {
                tabela.find("tr:gt(1)").remove() // ukloni sve redove u tabeli iza forme za pretragu

                var filmovi = odgovor.filmovi
                for (var itFilm in filmovi) {
                    tabela.append( // kreiraj, popuni red i dodaj ga u tabelu
                        '<tr>' +
                        '<td class="broj">' + (parseInt(itFilm) + 1) + '</td>' +
                        '<td><a href="film.html?id=' + filmovi[itFilm].id + '">' + filmovi[itFilm].naziv + '</a></td>' +
                        '<td><ul></ul></td>' + // lista u redu
                        '<td class="broj">' + filmovi[itFilm].trajanje + '</td>' +
                        '<td>' +
                        '<a href="projekcije.html?filmId=' + filmovi[itFilm].id + '">projekcije</a>' +
                        '</td>' +
                        '</tr>'
                    )
                    var lista = tabela.find("tr").eq(parseInt(itFilm) + 2).find("ul") // pronađi listu u redu

                    var zanroviFilma = filmovi[itFilm].zanrovi
                    for (var itZanr in zanroviFilma) { // kreiraj stavke u listi i dodaj ih u listu
                        lista.append(
                            '<li><a href="zanr.html?id=' + zanroviFilma[itZanr].id + '">' + zanroviFilma[itZanr].naziv + '</a></li>'
                        )
                    }
                }
            }
        })
        console.log("GET: " + "Filmovi")
    }
    */



    $("#formaDodajKomentar").submit(function(e) { // registracija handler-a za pretragu

        e.preventDefault();
        $.ajax({
            url: "/Trening/api/komentari",
            type: "post", //send it through get method
            data: {
                tekst: $("#tekst").val(),
                ocena: $("#ocena").val(),
                anonimanStr: $("#anoniman").is(':checked') ? "anoniman": "javan",
                treningId:$("#treningId").val()


            },
            success: function(response) {
                $("#poruka").empty();
                $("#poruka").html("Uspešno dodat komentar");

            },
            error: function(xhr) {
                //Do Something to handle error
            }
        });
        return false // sprečiti da submit forme promeni stranicu
    })


})