DROP SCHEMA IF EXISTS trening;
CREATE SCHEMA trening DEFAULT CHARACTER SET utf8;
USE trening;
CREATE  TABLE tipTreninga(
    id BIGINT AUTO_INCREMENT,
    ime VARCHAR(20) NOT NULL,
    opis TEXT NOT NULL,
    PRIMARY  KEY(id)


);

CREATE TABLE trening(
    id BIGINT AUTO_INCREMENT,
    naziv VARCHAR(20) NOT NULL,
    treneri VARCHAR(20) NOT NULL,
    opis VARCHAR(100) NOT NULL,
    slika VARCHAR(150) NOT NULL,
    tipId BIGINT NOT NULL,
    cena DECIMAL NOT NULL,
    vrsta ENUM('POJEDINACNI', 'GRUPNI') NOT NULL,
    nivo ENUM('AMATERSKI', 'SREDNJI', 'NAPREDNI') NOT NULL,
    trajanje INT NOT NULL,
	PRIMARY KEY(id),
    FOREIGN KEY(tipId) REFERENCES tipTreninga(id));





CREATE TABLE korisnik (

    korisnickoIme VARCHAR(20) NOT NULL,
       lozinka VARCHAR(20) NOT NULL,
       email VARCHAR(20) NOT NULL,
        ime VARCHAR(20) NOT NULL,
        prezime VARCHAR(20) NOT NULL,
        datumRodjenja DATE NOT NULL,
        adresa VARCHAR(100) NOT  NULL,
        brojTelefona VARCHAR(20) NOT NULL,
        datumVremeRegistracije DATETIME NOT NULL,
        uloga ENUM('CLAN', 'ADMINISTRATOR') DEFAULT 'CLAN' NOT NULL,
        blokiran BOOL DEFAULT FALSE NOT NULL,
       PRIMARY KEY(korisnickoIme)
);

CREATE  TABLE sala(
    id BIGINT AUTO_INCREMENT,
    oznaka VARCHAR(20) NOT NULL,
    kapacitet INT NOT NULL,
    PRIMARY  KEY(id)
);



CREATE TABLE clanskaKartica(
    id BIGINT AUTO_INCREMENT,
    popust INT NOT NULL,
    brojPoena INT NOT NULL,
    status ENUM('NA_CEKANJU', 'ODOBREN', 'NIJE_ODOBREN') DEFAULT 'NA_CEKANJU' NOT NULL,
    korisnickoIme VARCHAR(20) NOT NULL,
    FOREIGN KEY(korisnickoIme) REFERENCES korisnik(korisnickoIme)
        ON DELETE CASCADE,

    PRIMARY  KEY(id)

);

CREATE TABLE terminTreninga(
    id BIGINT AUTO_INCREMENT,
    salaId BIGINT NOT NULL,
    treningId BIGINT NOT NULL,
    datum DATETIME NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(salaId) REFERENCES sala(id)
        ON DELETE CASCADE,
    FOREIGN KEY(treningId) REFERENCES trening(id)
        ON DELETE CASCADE
);

CREATE TABLE komentar(
    id BIGINT AUTO_INCREMENT,
    tekst TEXT NOT NULL,
    ocena INT NOT NULL,
    datum DATE NOT NULL,
    korisnickoIme VARCHAR(20) NOT NULL,
    treningId BIGINT NOT NULL,
    status ENUM('NA_CEKANJU', 'ODOBREN', 'NIJE_ODOBREN') DEFAULT 'NA_CEKANJU' NOT NULL,
    anoniman BOOL default FALSE NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(korisnickoIme) REFERENCES korisnik(korisnickoIme)
        ON DELETE CASCADE,
    FOREIGN KEY(treningId) REFERENCES trening(id)
        ON DELETE CASCADE


);
CREATE TABLE rezervacija(
                        id BIGINT AUTO_INCREMENT,
                         terminTreningaId BIGINT NOT NULL,
                         korisnickoIme VARCHAR(20) NOT NULL,
                        datumVreme DATETIME NOT NULL,
                        cena DECIMAL NOT NULL,
                        PRIMARY KEY(id),

                         FOREIGN KEY(korisnickoIme) REFERENCES korisnik(korisnickoIme)
                             ON DELETE CASCADE,
                         FOREIGN KEY(terminTreningaId) REFERENCES terminTreninga(id)
                             ON DELETE CASCADE


);

CREATE TABLE listaZelja(
                            id BIGINT AUTO_INCREMENT,
                            treningId BIGINT NOT NULL,
                            korisnickoIme VARCHAR(20) NOT NULL,
                            PRIMARY KEY(id),

                            FOREIGN KEY(korisnickoIme) REFERENCES korisnik(korisnickoIme)
                                ON DELETE CASCADE,
                            FOREIGN KEY(treningId) REFERENCES trening(id)
                                ON DELETE CASCADE


);
CREATE TABLE specijalniDatum(
                           id BIGINT AUTO_INCREMENT,
                          datum DATE NOT NULL,
                           popust INT NOT NULL,
                           treningId BIGINT,
                           PRIMARY KEY(id),
                           FOREIGN KEY(treningId) REFERENCES trening(id)
                               ON DELETE CASCADE

);

INSERT INTO tipTreninga(id,ime,opis)
VALUES(8,'GymPro','Rad sa tegovima');

INSERT INTO trening(id,naziv, treneri, opis, slika, tipid, cena, vrsta, nivo, trajanje)
VALUES(333,'Crossfit','Milos Trener','opisTreninga','slika',8,'1000','POJEDINACNI','AMATERSKI',35);


INSERT INTO korisnik (korisnickoIme, lozinka, email, ime, prezime,datumRodjenja,adresa,brojTelefona,datumVremeRegistracije,uloga)
 VALUES ('JohnSmith','test123','johnsmith@gmail.com','John','Smith','2001-12-28','Bulevar BB','061234567','2021-12-28T19:13:25.020188','ADMINISTRATOR');


INSERT INTO sala(id,oznaka,kapacitet)
VALUES(9,'oznaka',25);



INSERT INTO clanskaKartica(id,popust,brojPoena)
VALUES(10,5,10);


INSERT INTO terminTreninga(id,treningId,salaId,datum)
VALUES(12,333,9,'2021-12-27');


INSERT INTO komentar(id,tekst,ocena,datum,korisnickoIme,treningId,status,anoniman)
VALUES(41,'tekstkomentara',5,'2021-12-28','JohnSmith',333,'NA_CEKANJU',0);
