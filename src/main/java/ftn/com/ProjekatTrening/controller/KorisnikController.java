package ftn.com.ProjekatTrening.controller;

import ftn.com.ProjekatTrening.model.EUloga;
import ftn.com.ProjekatTrening.model.Korisnik;
import ftn.com.ProjekatTrening.model.ListaZelja;
import ftn.com.ProjekatTrening.model.Rezervacija;
import ftn.com.ProjekatTrening.service.KorisnikService;
import ftn.com.ProjekatTrening.service.ListaZeljaService;
import ftn.com.ProjekatTrening.service.RezervacijaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.rmi.server.ExportException;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping(value="/Korisnici")
public class KorisnikController {

	public static final String KORISNIK_KEY = "prijavljeniKorisnik";
	
	@Autowired
	private KorisnikService korisnikService;


	@Autowired
	private RezervacijaService rezervacijaService;


	@Autowired
	private ListaZeljaService listaZeljaService;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private LocaleResolver localeResolver;


	@PostConstruct
	public void init() {	
		baseURL = servletContext.getContextPath() + "/";
	}


	@GetMapping(value="/Register")
	public ModelAndView getRegister(){
		ModelAndView mw = new ModelAndView("registracija");
		return mw;

	}

	@GetMapping(value="/Login")
	public ModelAndView getLogin(){
		ModelAndView mw = new ModelAndView("prijava");
		return mw;

	}

	@PostMapping(value="/Register")
	public ModelAndView register(@RequestParam String korisnickoIme, @RequestParam String lozinka,
								 @RequestParam String email, @RequestParam String ime, @RequestParam String prezime,
								 @RequestParam  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate datumRodjenja,
								 @RequestParam String ponovljenaLozinka, @RequestParam String adresa,
								 @RequestParam String brojTelefona,

								 HttpSession session, HttpServletResponse response, HttpServletRequest request) throws IOException {
		Locale lokalizacija = localeResolver.resolveLocale(request);
		try {

			// validacija
			Korisnik postojeciKorisnik = korisnikService.findOne(korisnickoIme);

			if (postojeciKorisnik != null) {
				String poruka = messageSource.getMessage("korisnik.usernameExists",
						null, lokalizacija);
				throw new Exception(poruka);
			}
			if (korisnickoIme.equals("") || lozinka.equals("")) {
				String poruka = messageSource.getMessage("korisnik.usernameAndPass",
						null, lokalizacija);
				throw new Exception(poruka);
			}
			if (!lozinka.equals(ponovljenaLozinka)) {
				String poruka = messageSource.getMessage("korisnik.passwordsAreNotEqual",
						null, lokalizacija);
				throw new Exception(poruka);
			}
			if (email.equals("")) {
				String poruka = messageSource.getMessage("korisnik.emailShouldntBeEmpty",
						null, lokalizacija);
				throw new Exception(poruka);
			}
			if (ime.equals("")){
				String poruka = messageSource.getMessage("korisnik.emptyName",
						null, lokalizacija);
				throw new Exception(poruka);
			}
			if (prezime.equals("")){
				String poruka = messageSource.getMessage("korisnik.emptySurname",
						null, lokalizacija);
				throw new Exception(poruka);
			}
			if (adresa.equals("")){
				String poruka = messageSource.getMessage("korisnik.adressEmpty",
						null, lokalizacija);
				throw new Exception(poruka);
			}
			if (datumRodjenja.equals("")){
				String poruka = messageSource.getMessage("korisnik.birtDateEmpty",
						null, lokalizacija);
				throw new Exception(poruka);
			}
			if (brojTelefona.equals("")){
				String poruka = messageSource.getMessage("korisnik.phoneNumEmpty",
						null, lokalizacija);
				throw new Exception(poruka);
			}

			// registracija
			Korisnik korisnik = new Korisnik(korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, EUloga.CLAN, false);
			korisnikService.save(korisnik);
			response.sendRedirect(baseURL + "Korisnici/Login");

			return null;
		} catch (Exception ex) {
			// ispis greške
			ex.printStackTrace();
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešna registracija!";
			}

			// prosleđivanje
			ModelAndView rezultat = new ModelAndView("registracija");
			rezultat.addObject("poruka", poruka);

			return rezultat;
		}
	}
	
	@PostMapping(value="/Login")
	public ModelAndView postLogin(@RequestParam String korisnickoIme, @RequestParam String lozinka, 
			HttpSession session, HttpServletResponse response) throws IOException {
		try {
			// validacija
			Korisnik korisnik = korisnikService.findOne(korisnickoIme, lozinka);
			if (korisnik == null) {
				throw new Exception("Neispravno korisničko ime ili lozinka!");
			}
			if(korisnik.isBlokiran()){
				throw new Exception("Nemoguća prijava, blokirani ste!");
			}
			// prijava
			session.setAttribute(KorisnikController.KORISNIK_KEY, korisnik);
			response.sendRedirect(baseURL + "treninzi");
			return null;

		} catch (Exception ex) {
			// ispis greške
			ex.printStackTrace();
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešna prijava!";
			}
			
			// prosleđivanje
			ModelAndView rezultat = new ModelAndView("prijava");
			rezultat.addObject("poruka", poruka);

			return rezultat;
		}
	}

	@GetMapping(value="/Logout")
	public void logout(HttpSession session, HttpServletResponse response) throws IOException {
		// odjava	
		session.invalidate();
		
		response.sendRedirect(baseURL + "Korisnici/Login");
	}

	@GetMapping("/korisnici")
	public ModelAndView getKorisnici(@RequestParam(defaultValue = "") String korisnickoIme, @RequestParam(defaultValue = "") String uloga, @RequestParam(defaultValue = "") String uredjenost, @RequestParam(defaultValue = "") String sortiranje){
		ModelAndView mv = new ModelAndView("korisnici");
		EUloga euloga = null;
		if(!uloga.isBlank()){
			euloga = EUloga.valueOf(uloga);
		}
		List<Korisnik> korisnici = korisnikService.findByKorisnickoImeAndUloga(korisnickoIme, euloga, sortiranje, uredjenost);
		mv.addObject("korisnici", korisnici);
		return mv;
	}

	@GetMapping("/profil/{korisnickoIme}")
	public ModelAndView getProfil(@PathVariable String korisnickoIme, @RequestParam(defaultValue = "") String poruka){
		ModelAndView mv = new ModelAndView("profil");
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		List<Rezervacija> rezervacije = rezervacijaService.findByKorisnik(korisnickoIme);
		double ukupnaCena = 0;
		for(Rezervacija rezervacija : rezervacije){
			ukupnaCena += rezervacija.getTerminTreninga().getTrening().getCena();

		}
		List<ListaZelja> listaZelja = listaZeljaService.findByKorisnik(korisnickoIme);
		mv.addObject("korisnik", korisnik);
		mv.addObject("rezervacije", rezervacije);
		mv.addObject("ukupnaCena", ukupnaCena);
		mv.addObject("listaZelja", listaZelja);
		mv.addObject("poruka", poruka);
		return mv;
	}

	@PostMapping("/profil/{korisnickoIme}")
	public String postProfil(@PathVariable String korisnickoIme, @RequestParam String ime, @RequestParam String prezime, @RequestParam String email, @RequestParam String adresa, @RequestParam(defaultValue = "") String lozinka, @RequestParam(defaultValue = "") String lozinkaPotvrda, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate datumRodjenja, @RequestParam String brojTelefona){
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		korisnik.setIme(ime);
		korisnik.setPrezime(prezime);
		korisnik.setEmail(email);
		korisnik.setAdresa(adresa);
		korisnik.setKorisnickoIme(korisnickoIme);
		korisnik.setDatumRodjenja(datumRodjenja);
		korisnik.setBrojTelefona(brojTelefona);


		if(!lozinka.isBlank()){
			if(lozinka.equals(lozinkaPotvrda)){
				korisnik.setLozinka(lozinka);
			}
		}
		korisnikService.update(korisnik);

		return "redirect:/Korisnici/profil/"+korisnickoIme;
	}


	@PostMapping("/blokiraj")
	public String postBlokiraj(@RequestParam String korisnickoIme){
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		korisnik.setBlokiran(true);
		korisnikService.update(korisnik);

		return "redirect:/Korisnici/profil/"+korisnickoIme;

	}

	@PostMapping("/odblokiraj")
	public String postOdblokiraj(@RequestParam String korisnickoIme){
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		korisnik.setBlokiran(false);
		korisnikService.update(korisnik);
		return "redirect:/Korisnici/profil/"+korisnickoIme;

	}

	@PostMapping("/promenaUloge")
	public String promenaUloga(@RequestParam String korisnickoIme, @RequestParam String uloga){
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		korisnik.setUloga(EUloga.valueOf(uloga));
		korisnikService.update(korisnik);
		return "redirect:/Korisnici/profil/"+korisnickoIme;
	}


	
}
