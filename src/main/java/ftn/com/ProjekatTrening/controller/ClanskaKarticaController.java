package ftn.com.ProjekatTrening.controller;

import ftn.com.ProjekatTrening.model.ClanskaKartica;
import ftn.com.ProjekatTrening.model.EStatusClanskeKartice;
import ftn.com.ProjekatTrening.model.EStatusClanskeKartice;
import ftn.com.ProjekatTrening.model.Korisnik;
import ftn.com.ProjekatTrening.service.ClanskaKarticaService;
import ftn.com.ProjekatTrening.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class ClanskaKarticaController {
    @Autowired
    private ClanskaKarticaService clanskaKarticaService;
    
    @Autowired
    private KorisnikService korisnikService;

    @GetMapping("/clanskaKartica/dodaj")
    private ModelAndView getClanskaKartica(HttpSession session){
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

        ModelAndView mv = new ModelAndView("zahtevClanskaKartica");
        boolean neMozePodneti = clanskaKarticaService.postojiPrihvacenIliNaCekanjuZaKorisnika(prijavljeniKorisnik.getKorisnickoIme());
        mv.addObject("neMozePodneti", neMozePodneti);
        return mv;
    }
    @PostMapping("/clanskaKartica/dodaj")
    private String postClanskaKartica(HttpSession session){
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

        ClanskaKartica clanskaKartica = new ClanskaKartica(0, 10, EStatusClanskeKartice.NA_CEKANJU, prijavljeniKorisnik);
        clanskaKarticaService.save(clanskaKartica);
        return "redirect:/clanskaKartica/dodaj";
    }
    
    @GetMapping("/clanskaKartica/odobravanje")
    private ModelAndView getOdobravanjeClanskaKartica(){
        List<ClanskaKartica> clanskeKartice = clanskaKarticaService.nadjiPoStatusu(EStatusClanskeKartice.NA_CEKANJU);
        ModelAndView mv = new ModelAndView("clanskaKartica");
        mv.addObject("clanskeKartice", clanskeKartice);
        return mv;
    }

    @PostMapping("/clanskaKartica/odobri")
    public String getClanskaKarticaOdobri(@RequestParam int id){
        clanskaKarticaService.promeniStatus(id, EStatusClanskeKartice.ODOBREN);
        return "redirect:/clanskaKartica/odobravanje";
    }

    @PostMapping("/clanskaKartica/odbij")
    public String getClanskaKarticaOdbij(@RequestParam int id){
        clanskaKarticaService.promeniStatus(id, EStatusClanskeKartice.NIJE_ODOBREN);
        return "redirect:/clanskaKartica/odobravanje";
    }


}
