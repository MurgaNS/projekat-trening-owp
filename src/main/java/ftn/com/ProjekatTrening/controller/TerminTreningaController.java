package ftn.com.ProjekatTrening.controller;

import ftn.com.ProjekatTrening.model.Korisnik;
import ftn.com.ProjekatTrening.model.Sala;
import ftn.com.ProjekatTrening.model.TerminTreninga;
import ftn.com.ProjekatTrening.model.Trening;
import ftn.com.ProjekatTrening.service.SalaService;
import ftn.com.ProjekatTrening.service.TerminTreningaService;
import ftn.com.ProjekatTrening.service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

@Controller
public class TerminTreningaController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private LocaleResolver localeResolver;

    @Autowired
    private TerminTreningaService terminTreningaService;

    @Autowired
    private TreningService treningService;

    @Autowired
    private SalaService salaService;

/*
    @PostMapping("/termini/rezervacija")
    public String postRezervacija(@RequestParam(name="terminId") int terminTreningaId, HttpSession session){
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

        terminTreningaService.rezervacija(terminTreningaId, prijavljeniKorisnik.getKorisnickoIme());

        return "redirect:/korpa";

    }
    */


    @GetMapping("/termini/dodaj")
    public ModelAndView getDodajTermin(@RequestParam(defaultValue = "") String poruka){
        ModelAndView mv = new ModelAndView("dodavanjeTermina");
        List<Sala> sale = salaService.findAll();
        List<Trening> treninzi = treningService.findAll();
        mv.addObject("sale", sale);
        mv.addObject("treninzi", treninzi);
        mv.addObject("poruka", poruka);
        return mv;
    }

    @PostMapping("/termini/dodaj")
    public String postDodajTermin(@RequestParam int salaId, @RequestParam int treningId, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  LocalDateTime datum, HttpServletRequest request){
        Sala sala = salaService.findOne(salaId);
        Trening trening = treningService.findOne(treningId);
        Locale lokalizacija = localeResolver.resolveLocale(request);


        LocalDateTime pocetniDatum = datum;
        LocalDateTime krajnjiDatum = datum.plusMinutes(trening.getTrajanje());

        if(terminTreningaService.findTerminTreningaBySalaAndDatum(sala, pocetniDatum, krajnjiDatum) != null){
            String poruka = messageSource.getMessage("salakontroler.preklapanje",
                    null, lokalizacija);
            return "redirect:/termini/dodaj?poruka=" + poruka;
        }


        TerminTreninga termin = new TerminTreninga(sala, trening, datum);
        terminTreningaService.save(termin);
        return "redirect:/termini/dodaj";
    }
}
