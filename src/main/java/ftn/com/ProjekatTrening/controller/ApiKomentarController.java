package ftn.com.ProjekatTrening.controller;


import ftn.com.ProjekatTrening.model.EStatusKomentara;
import ftn.com.ProjekatTrening.model.Komentar;
import ftn.com.ProjekatTrening.model.Korisnik;
import ftn.com.ProjekatTrening.model.Trening;
import ftn.com.ProjekatTrening.service.KomentarService;
import ftn.com.ProjekatTrening.service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiKomentarController {
    @Autowired
    private KomentarService komentarService;

    @Autowired
    private TreningService treningService;

    @GetMapping("/komentari")
    public List<Komentar> komentariZaTrening(@RequestParam long treningId){
        return komentarService.nadjiPoTreningId(treningId);
    }

    @PostMapping("/komentari")
    public ResponseEntity<Komentar> postKomentar(HttpSession session, @RequestParam String tekst, @RequestParam int ocena, @RequestParam(defaultValue="anoniman") String anonimanStr, @RequestParam long treningId){
        System.out.println("zahtev....");
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        Trening trening = treningService.findOne(treningId);
        boolean anoniman = anonimanStr.equals("anoniman");
        Komentar komentar = new Komentar(tekst, ocena, LocalDate.now(), prijavljeniKorisnik, trening, EStatusKomentara.NA_CEKANJU, anoniman);
        komentarService.save(komentar);
        return new ResponseEntity<>(komentar, HttpStatus.OK);
    }

    @GetMapping("/komentari/odobravanje")
    public List<Komentar> getKomentari(){
        return komentarService.nadjiPoStatusu(EStatusKomentara.NA_CEKANJU);
    }

    @PostMapping("/komentari/{id}/odobri")
    public ResponseEntity<Komentar> odobriKomentar(@PathVariable int id){
        komentarService.promeniStatus(id, EStatusKomentara.ODOBREN);

        return new ResponseEntity<>(new Komentar(), HttpStatus.OK);
    }

    @PostMapping("/komentari/{id}/odbij")
    public ResponseEntity<Komentar> odbijKomentar(@PathVariable int id){
        komentarService.promeniStatus(id, EStatusKomentara.NIJE_ODOBREN);

        return new ResponseEntity<>(new Komentar(), HttpStatus.OK);

    }
}
