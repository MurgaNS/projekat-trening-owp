package ftn.com.ProjekatTrening.controller;

import ftn.com.ProjekatTrening.model.Korisnik;
import ftn.com.ProjekatTrening.model.ListaZelja;
import ftn.com.ProjekatTrening.model.Trening;
import ftn.com.ProjekatTrening.service.ListaZeljaService;
import ftn.com.ProjekatTrening.service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class ListaZeljaController {

    @Autowired
    private ListaZeljaService listaZeljaService;

    @Autowired
    private TreningService treningService;

    @PostMapping("/listaZelja/otkazivanje")
    public String postOtkazivanje(@RequestParam long id, HttpSession session){
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

        listaZeljaService.delete(id);
        return "redirect:/Korisnici/profil/"+prijavljeniKorisnik.getKorisnickoIme();
    }

    @PostMapping("/listaZelja/dodaj")
    public String postDodaj(@RequestParam long treningId, HttpSession session){
        Trening trening = treningService.findOne(treningId);
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        ListaZelja zelja = new ListaZelja(trening, prijavljeniKorisnik);
        listaZeljaService.save(zelja);
        return "redirect:/treninzi/"+treningId;
    }
}
