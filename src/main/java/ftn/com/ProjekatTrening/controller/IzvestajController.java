package ftn.com.ProjekatTrening.controller;

import ftn.com.ProjekatTrening.model.Izvestaj;
import ftn.com.ProjekatTrening.model.Korisnik;
import ftn.com.ProjekatTrening.service.TerminTreningaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
public class IzvestajController {
    //SELECT trening.trening.naziv, trening.trening.treneri,count(trening.terminTreninga.id) as brojZakazanihTreninga, count(trening.terminTreninga.id)*sum(trening.trening.cena) as ukupnaCena FROM trening.termintreninga inner join trening.rezervacija inner join trening.trening on termintreninga.id = rezervacija.terminTreningaId and trening.id = termintreninga.treningId WHERE datumVreme >= '2022-02-17 19:00:00' GROUP BY trening.trening.id;}
    @Autowired
    private TerminTreningaService terminTreningaService;

    private String baseURL;

    @Autowired
    private ServletContext servletContext;

    @PostConstruct
    public void init() {
        baseURL = servletContext.getContextPath() + "/";
    }

    @GetMapping("/izvestaj")
    public ModelAndView getIzvestaj(HttpSession session, HttpServletResponse response) throws IOException {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        if(prijavljeniKorisnik == null || prijavljeniKorisnik.isClan()){
            response.sendRedirect(baseURL + "Korisnici/Login");

            return null;
        }
        ModelAndView mv = new ModelAndView("izvestaj");
        mv.addObject("izvestaji", new ArrayList<>());
        return mv;
    }
    @GetMapping("/izvestajPretraga")
    public ModelAndView getIzvestajPretraga(HttpSession session, HttpServletResponse response, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime pocetniDatum, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime krajnjiDatum) throws IOException {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        if(prijavljeniKorisnik == null || prijavljeniKorisnik.isClan()){
            response.sendRedirect(baseURL + "Korisnici/Login");

            return null;
        }
        List<Izvestaj> izvestaji = terminTreningaService.getIzvestaji(pocetniDatum, krajnjiDatum);
        int ukupanBrojZakazanih = 0;
        double ukupnaCena = 0;
        for(Izvestaj izvestaj : izvestaji){
            ukupanBrojZakazanih += izvestaj.getBrojZakazanihTreninga();
            ukupnaCena += izvestaj.getUkupnaCena();
        }
        ModelAndView mv = new ModelAndView("izvestaj");
        mv.addObject("izvestaji", izvestaji);
        mv.addObject("ukupanBrojZakazanih", ukupanBrojZakazanih);
        mv.addObject("ukupnaCena", ukupnaCena);
        return mv;
    }

}