package ftn.com.ProjekatTrening.controller;

import ftn.com.ProjekatTrening.model.*;
import ftn.com.ProjekatTrening.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
@Controller
public class TreningController implements ServletContextAware {

    @Autowired
    private TreningService treningService;

    @Autowired
    private TerminTreningaService terminTreningaService;

    @Autowired
    private TipTreningaService tipTreningaService;

    @Autowired
    private KomentarService komentarService;

    @Autowired
    private RezervacijaService rezervacijaService;

    @Autowired
    private ServletContext servletContext;
    private String baseURL;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @PostConstruct
    public void init() {
        baseURL = servletContext.getContextPath() + "/";
    }

    @GetMapping("/treninzi")
    public ModelAndView getAllTreninzi(@RequestParam(defaultValue="") String naziv, @RequestParam(defaultValue="-1") Integer tipId,
                                       @RequestParam(defaultValue = "-1") Double minCena, @RequestParam(defaultValue = "-1") Double maxCena,
                                       @RequestParam(defaultValue="") String treneri, @RequestParam(defaultValue = "") String vrsta,
                                       @RequestParam(defaultValue = "") String nivo, @RequestParam(defaultValue="") String sortiranje,
                                       @RequestParam(defaultValue="") String uredjenost){
        ModelAndView mv = new ModelAndView("treninzi");
        List<TipTreninga> tipovi = tipTreningaService.findAll();
        mv.addObject("tipovi", tipovi);
        List<Trening> treninzi = treningService.find(naziv, tipId, minCena, maxCena, treneri, vrsta, nivo, sortiranje, uredjenost);
        mv.addObject("treninzi", treninzi);
        System.out.println(sortiranje + " " + uredjenost);
        return mv;

    }
    @GetMapping("/treninzi/dodaj")
    public ModelAndView getTreningForma(HttpSession session, HttpServletResponse response) throws IOException {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
            response.sendRedirect(baseURL + "treninzi");
            return null;
        }
        List<TipTreninga> tipovi = tipTreningaService.findAll();
        ModelAndView mv = new ModelAndView("dodavanjeTreninga");
        mv.addObject("tipovi", tipovi);
        return mv;

    }

    @PostMapping("/treninzi/dodaj")
    public ModelAndView postTreningForma(@RequestParam String naziv, @RequestParam String opis, @RequestParam double cena,
                                         @RequestParam String treneri, @RequestParam int tipId, @RequestParam MultipartFile slika,
                                         @RequestParam String vrsta, @RequestParam String nivo, @RequestParam int trajanje, HttpSession session, HttpServletResponse response) throws IOException {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        if (!prijavljeniKorisnik.isAdministrator()) {
            response.sendRedirect(baseURL + "treninzi");
            return null;
        }
        EVrstaTreninga vrstaTreninga = EVrstaTreninga.valueOf(vrsta);
        ENivoTreninga nivoTreninga = ENivoTreninga.valueOf(nivo);
        TipTreninga tip = tipTreningaService.findOne(tipId);
        Trening trening = new Trening(naziv, treneri, opis, slika.getOriginalFilename(), tip, cena, vrstaTreninga, nivoTreninga, trajanje);

        treningService.save(trening);
        try {

           Path path = Path.of(System.getProperty("user.dir"), "src", "main", "resources", "static", "images", slika.getOriginalFilename());
            slika.transferTo(path.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }

        ModelAndView mv = new ModelAndView("dodavanjeTreninga");
        return mv;

    }

    @PostMapping("/treninzi/edit/{id}")
    public ModelAndView postTreningEdit(@PathVariable int id, @RequestParam String naziv, @RequestParam String opis, @RequestParam double cena,
                                        @RequestParam String treneri, @RequestParam int tipId, @RequestParam MultipartFile slika,
                                        @RequestParam String vrsta, @RequestParam String nivo, @RequestParam int trajanje, HttpSession session, HttpServletResponse response) throws IOException {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        if (!prijavljeniKorisnik.isAdministrator()) {
            response.sendRedirect(baseURL + "treninzi");
            return null;
        }
        Trening trening = treningService.findOne(id);

        EVrstaTreninga vrstaTreninga = EVrstaTreninga.valueOf(vrsta);
        ENivoTreninga nivoTreninga = ENivoTreninga.valueOf(nivo);
        TipTreninga tip = tipTreningaService.findOne(tipId);

        trening.setNaziv(naziv);
        trening.setOpis(opis);
        trening.setCena(cena);
        trening.setTreneri(treneri);
        trening.setTrajanje(trajanje);
        trening.setOpis(opis);
        trening.setTip(tip);
        trening.setVrsta(vrstaTreninga);
        trening.setNivo(nivoTreninga);


        if(!slika.isEmpty()) {
            trening.setSlika(slika.getOriginalFilename());

            try {
                Path path = Path.of(System.getProperty("user.dir"), "src", "main", "resources", "static", "images", slika.getOriginalFilename());
                slika.transferTo(path.toFile());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        treningService.update(trening);

        ModelAndView mv = new ModelAndView("trening");
        mv.addObject("trening", trening);
        List<TipTreninga> tipovi = tipTreningaService.findAll();
        mv.addObject("tipovi", tipovi);
        return mv;

    }


    @GetMapping("/treninzi/{id}")
    public ModelAndView getOneTrening(@PathVariable int id, HttpSession session, HttpServletResponse response) throws IOException {
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        ModelAndView mv = new ModelAndView("trening");
        Trening trening = treningService.findOne(id);
        List<TipTreninga> tipovi = tipTreningaService.findAll();
        List<TerminTreninga> termini = terminTreningaService.aktuelniTerminiZaTrening(id);
        List<Komentar> komentari = komentarService.nadjiPoTreningId(id);
        boolean mozeKomentarisati = rezervacijaService.existsByKorisnikAndTrening(prijavljeniKorisnik.getKorisnickoIme(), trening);
        mv.addObject("tipovi", tipovi);
        mv.addObject("trening", trening);
        mv.addObject("termini", termini);
        mv.addObject("komentari", komentari);
        mv.addObject("mozeKomentarisati", mozeKomentarisati);
        return mv;
    }

    @PostMapping("/treninzi/delete/{id}")
    public void deleteTraining(@PathVariable int id){

    }


}
