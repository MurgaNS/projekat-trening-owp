package ftn.com.ProjekatTrening.controller;

import ftn.com.ProjekatTrening.model.EStatusKomentara;
import ftn.com.ProjekatTrening.model.Komentar;
import ftn.com.ProjekatTrening.model.Korisnik;
import ftn.com.ProjekatTrening.model.Trening;
import ftn.com.ProjekatTrening.service.KomentarService;
import ftn.com.ProjekatTrening.service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.List;

@Controller
public class KomentarController {

    @Autowired
    private KomentarService komentarService;

    @Autowired
    private TreningService treningService;

    @PostMapping("/komentar/dodaj")
    public String postKomentar(HttpSession session, @RequestParam String tekst, @RequestParam int ocena, @RequestParam(defaultValue="anoniman") String anonimanStr, @RequestParam long treningId){
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        Trening trening = treningService.findOne(treningId);
        boolean anoniman = anonimanStr.equals("anoniman");
        Komentar komentar = new Komentar(tekst, ocena, LocalDate.now(), prijavljeniKorisnik, trening, EStatusKomentara.NA_CEKANJU, anoniman);
        komentarService.save(komentar);
        return "redirect:/treninzi/"+treningId;
    }

    @GetMapping("/komentar/odobravanje")
    public ModelAndView getKomentariOdobravanje(){
        ModelAndView mv = new ModelAndView("komentari");
        List<Komentar> komentari = komentarService.nadjiPoStatusu(EStatusKomentara.NA_CEKANJU);
        mv.addObject("komentari", komentari);
        return mv;
    }



    @PostMapping("/komentar/odobri")
    public String getKomentarOdobri(@RequestParam int id){
        komentarService.promeniStatus(id, EStatusKomentara.ODOBREN);
        return "redirect:/komentar/odobravanje";
    }

    @PostMapping("/komentar/odbij")
    public String getKomentarOdbij(@RequestParam int id){
        komentarService.promeniStatus(id, EStatusKomentara.NIJE_ODOBREN);
        return "redirect:/komentar/odobravanje";
    }
}
