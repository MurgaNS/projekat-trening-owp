package ftn.com.ProjekatTrening.controller;

import ftn.com.ProjekatTrening.model.Sala;
import ftn.com.ProjekatTrening.service.SalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

@Controller
public class SalaController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private LocaleResolver localeResolver;

    @Autowired
    private SalaService salaService;

    @GetMapping("/sale")
    public ModelAndView getSale(@RequestParam(defaultValue = "") String poruka, @RequestParam(defaultValue = "") String oznakaPretraga, @RequestParam(defaultValue = "Rastuce") String uredjenost){
        List<Sala> sale = salaService.find(oznakaPretraga, uredjenost);

        ModelAndView mv = new ModelAndView("sale");
        mv.addObject("sale", sale);
        mv.addObject("poruka", poruka);
        return mv;

    }

    @GetMapping("/sale/dodaj")
    public ModelAndView getForma(){
        ModelAndView mv = new ModelAndView("dodavanjeSale");
        return mv;
    }

    @PostMapping("/sale/dodaj")
    public ModelAndView postForma(@RequestParam String oznaka, @RequestParam int kapacitet){
        Sala sala = new Sala(oznaka, kapacitet);
        salaService.save(sala);
        ModelAndView mv = new ModelAndView("dodavanjeSale");
        return mv;


    }

    @GetMapping("/sale/edit/{id}")
    public ModelAndView getIzmeni(@PathVariable long id){
        Sala sala = salaService.findOne(id);
        ModelAndView mv = new ModelAndView("sala");
        mv.addObject("sala", sala);
        return mv;
    }

    @PostMapping("/sale/edit/{id}")
    public  ModelAndView postIzmeni(@PathVariable long id, @RequestParam int kapacitet){
        Sala sala = salaService.findOne(id);
        sala.setKapacitet(kapacitet);
        salaService.update(sala);
        ModelAndView mv = new ModelAndView("sala");
        mv.addObject("sala", sala);
        return mv;

    }

    @PostMapping("/sale/obrisi/{id}")
    public String postDelete(@PathVariable long id, HttpServletRequest request){
        Locale lokalizacija = localeResolver.resolveLocale(request);

        if(salaService.existsRezervacija(id)){
            String poruka = messageSource.getMessage("salakontroler.rezPostoji",
                    null, lokalizacija);
            return "redirect:/sale?poruka="+poruka;
        }
        salaService.delete(id);
        return "redirect:/sale";
    }
}
