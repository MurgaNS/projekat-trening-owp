package ftn.com.ProjekatTrening.controller;

import ftn.com.ProjekatTrening.model.*;
import ftn.com.ProjekatTrening.service.ClanskaKarticaService;
import ftn.com.ProjekatTrening.service.RezervacijaService;
import ftn.com.ProjekatTrening.service.SpecijalniDatumService;
import ftn.com.ProjekatTrening.service.TerminTreningaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Locale;

@Controller
public class RezervacijaController {

    @Autowired
    private RezervacijaService rezervacijaService;

    @Autowired
    private TerminTreningaService terminTreningaService;

    @Autowired
    private SpecijalniDatumService specijalniDatumService;

    @Autowired
    private ClanskaKarticaService clanskaKarticaService;


    @Autowired
    private MessageSource messageSource;

    @Autowired
    private LocaleResolver localeResolver;



    @PostMapping("/termini/rezervacija")
    private String postRezervacija(@RequestParam(name="terminId") int terminTreningaId, HttpServletRequest request, @RequestParam(defaultValue = "0") int brojPoenaKoristi, HttpSession session){
        Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        TerminTreninga terminTreninga = terminTreningaService.findOne(terminTreningaId);
        SpecijalniDatum specijalniDatum = specijalniDatumService.findByDatumAndTrening(LocalDate.now(), terminTreninga.getTrening());
        if(specijalniDatum == null){
            specijalniDatum = specijalniDatumService.findByDatum(LocalDate.now());
        }
        ClanskaKartica clanskaKartica = clanskaKarticaService.findByKorisnik(prijavljeniKorisnik.getKorisnickoIme());
        Locale lokalizacija = localeResolver.resolveLocale(request);

        if(brojPoenaKoristi > 5){
            String poruka = messageSource.getMessage("rezervacija.petPoena",
                    null, lokalizacija);
            return "redirect:/korpa?poruka="+poruka;

        }
        if(rezervacijaService.findByKorisnikAndTerminTreninga(prijavljeniKorisnik.getKorisnickoIme(), terminTreningaId) != null){
            String poruka = messageSource.getMessage("rezervacija.termin",
                    null, lokalizacija);
            return "redirect:/korpa?poruka="+poruka;

        }
        if(terminTreninga.getSala().getKapacitet() <= rezervacijaService.countTerminiTreninga(terminTreningaId)){
            String poruka = messageSource.getMessage("rezervacija.kapacitetSale",
                    null, lokalizacija);
            return "redirect:/korpa?poruka="+poruka;
        }
        List<Rezervacija> rez = rezervacijaService.findByKorisnikAndDatum(prijavljeniKorisnik.getKorisnickoIme(), terminTreninga.getDatum());
        System.out.println(rez.size());
        if(rezervacijaService.findByKorisnikAndDatum(prijavljeniKorisnik.getKorisnickoIme(), terminTreninga.getDatum()).size()!=0){
            String poruka = messageSource.getMessage("rezervacija.terminRezUToVreme",
                    null, lokalizacija);
            return "redirect:/korpa?poruka="+poruka;

        }
        double cena = terminTreninga.getTrening().getCena();
        if(specijalniDatum != null){
            if((specijalniDatum.getTrening() != null && specijalniDatum.getTrening().getId() == terminTreninga.getTrening().getId())
            || specijalniDatum.getTrening() == null) {
                cena = cena * (1 - specijalniDatum.getPopust() / 100.0);
            }
        }
        else{
            if(clanskaKartica != null) {
                if(clanskaKartica.getBrojPoena() < brojPoenaKoristi){
                    String poruka = messageSource.getMessage("rezervacija.nemaPoena",
                            null, lokalizacija);
                    return "redirect:/korpa?poruka="+poruka;
                }
                int brojPoena = (int) cena / 500;
                int brojPoenaPromena = brojPoena - brojPoenaKoristi;
                clanskaKartica.setBrojPoena(clanskaKartica.getBrojPoena() + brojPoenaPromena);
                clanskaKarticaService.update(clanskaKartica);
            }


        }
        Rezervacija rezervacija = new Rezervacija(terminTreninga, prijavljeniKorisnik,  cena);
        rezervacijaService.save(rezervacija);
        String poruka = messageSource.getMessage("rezervacija.uspesnoRez",
                null, lokalizacija);
        return "redirect:/korpa?poruka="+poruka;
    }

    @GetMapping("/rezervacije/{id}")
    private ModelAndView getRezervacija(@PathVariable long id){
        Rezervacija rezervacija = rezervacijaService.findOne(id);
        ModelAndView mv = new ModelAndView("rezervacijaStranica");
        mv.addObject("rezervacija", rezervacija);
        return mv;
    }
    
    @PostMapping("/rezervacije/otkazivanje")
    private String postOtkazivanje(@RequestParam long id){
        Rezervacija rezervacija = rezervacijaService.findOne(id);
        LocalDateTime datumVremeRezervacije = rezervacija.getTerminTreninga().getDatum();
        LocalDateTime trenutnoDatumVreme = LocalDateTime.now();
        long brojSati = Duration.between(trenutnoDatumVreme, datumVremeRezervacije).getSeconds()/3600;
        if(brojSati < 24){
            return "redirect:/Korisnici/profil/"+rezervacija.getKorisnik().getKorisnickoIme()+"?poruka=Ne mozete otkazati termin(pocinje za manje od 24h.)";

        }
        rezervacijaService.delete(id);
        return "redirect:/Korisnici/profil/"+rezervacija.getKorisnik().getKorisnickoIme();
    }
}
