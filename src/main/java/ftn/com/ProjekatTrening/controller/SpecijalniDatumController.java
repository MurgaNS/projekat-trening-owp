package ftn.com.ProjekatTrening.controller;

import ftn.com.ProjekatTrening.model.SpecijalniDatum;
import ftn.com.ProjekatTrening.model.Trening;
import ftn.com.ProjekatTrening.service.SpecijalniDatumService;
import ftn.com.ProjekatTrening.service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;

@Controller
public class SpecijalniDatumController {

    @Autowired
    private SpecijalniDatumService specijalniDatumService;

    @Autowired
    private TreningService treningService;

    @GetMapping("/specijalniDatum")
    public ModelAndView getSpecijalniDatum(){
        ModelAndView mv = new ModelAndView("specijalniDatum");
        mv.addObject("treninzi", treningService.findAll());
        return mv;
    }

    @PostMapping("/specijalniDatum")
    public String postSpecijalniDatum( @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate datum, @RequestParam int popust, @RequestParam(defaultValue="0") long treningId){
        Trening trening = treningService.findOne(treningId);
        SpecijalniDatum specijalniDatum = new SpecijalniDatum(datum, popust, trening);
        specijalniDatumService.save(specijalniDatum);
        return "redirect:/specijalniDatum";
    }

}
