package ftn.com.ProjekatTrening.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

@Controller
public class InternacionalizacijaController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private LocaleResolver localeResolver;

    @GetMapping("/menjajLokalizacijuNaSrpski")
    public String naSrpski(HttpServletRequest request, HttpServletResponse response) {

        System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na sr");
        localeResolver.setLocale(request, response, Locale.forLanguageTag("sr"));

        return "redirect:/treninzi";
    }

    @GetMapping("/menjajLokalizacijuNaEngleski")
    public String naEngleski(HttpServletRequest request, HttpServletResponse response) {

        System.out.println("postavljanje lokalizacije za sesiju iz kontrolera na en");
        localeResolver.setLocale(request, response, Locale.ENGLISH);

        return "redirect:/treninzi";
    }

}
