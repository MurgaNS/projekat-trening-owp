package ftn.com.ProjekatTrening.controller;

import ftn.com.ProjekatTrening.model.TerminTreninga;
import ftn.com.ProjekatTrening.model.Trening;
import ftn.com.ProjekatTrening.service.KorpaService;
import ftn.com.ProjekatTrening.service.TerminTreningaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class KorpaController {

    @Autowired
    private TerminTreningaService terminTreningaService;

    @Autowired
    private KorpaService korpaService;


    @GetMapping(value="/korpa")
    public ModelAndView getKorpa(HttpSession session, @RequestParam(defaultValue = "") String poruka){
        List<TerminTreninga> treninzi = korpaService.nadjiSve(session);
        ModelAndView mv = new ModelAndView("korpa");
        mv.addObject("termini", treninzi);
        mv.addObject("poruka", poruka);
        return mv;
    }

    @PostMapping(value="/korpa")
    public String postKorpa(HttpSession session, @RequestParam long terminId){
          TerminTreninga terminTreninga =   terminTreningaService.findOne(terminId);
          if(!korpaService.postoji(session, terminId)){
              korpaService.dodaj(session, terminTreninga);
          }
          return "redirect:/korpa";

    }
    @PostMapping(value="/korpa/obrisi")
    public String postObrisi(HttpSession session, @RequestParam long terminId){
        korpaService.obrisi(session, terminId);
        return "redirect:/korpa";
    }
}
