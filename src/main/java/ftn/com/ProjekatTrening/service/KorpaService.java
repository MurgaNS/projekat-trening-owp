package ftn.com.ProjekatTrening.service;

import ftn.com.ProjekatTrening.model.TerminTreninga;
import ftn.com.ProjekatTrening.model.Trening;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface KorpaService {

    List<TerminTreninga> nadjiSve(HttpSession session);
    void dodaj(HttpSession session, TerminTreninga terminTreninga);
    void obrisi(HttpSession session, long id);
    boolean postoji(HttpSession session, long id);
}
