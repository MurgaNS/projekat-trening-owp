package ftn.com.ProjekatTrening.service;

import ftn.com.ProjekatTrening.model.Izvestaj;
import ftn.com.ProjekatTrening.model.Sala;
import ftn.com.ProjekatTrening.model.TerminTreninga;

import java.time.LocalDateTime;
import java.util.List;

public interface TerminTreningaService {
    public TerminTreninga findOne(long id);

    public List<TerminTreninga> findAll();

    /*
    public List<TerminTreninga> find(String korisnickoIme, String eMail, String pol, Boolean administrator);

     */

    public void save(TerminTreninga terminTreninga);

    public void update(TerminTreninga terminTreninga);

    public void delete(long id);


    public List<TerminTreninga> aktuelniTerminiZaTrening(long treningId);

    public void rezervacija(int terminTreningaId, String korisnickoIme);


    public List<Izvestaj> getIzvestaji(LocalDateTime pocetniDatum, LocalDateTime krajnjiDatum);

    public TerminTreninga findTerminTreningaBySalaAndDatum(Sala sala, LocalDateTime pocetniDatum, LocalDateTime krajnjiDatum);



}
