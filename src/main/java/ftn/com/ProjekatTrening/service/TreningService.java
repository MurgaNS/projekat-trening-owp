package ftn.com.ProjekatTrening.service;

import ftn.com.ProjekatTrening.model.Trening;

import java.util.List;

public interface TreningService {
    public Trening findOne(long id);

    public List<Trening> findAll();

    /*
    public List<Trening> find(String korisnickoIme, String eMail, String pol, Boolean administrator);

     */

    public void save(Trening trening);

    public void update(Trening trening);

    public void delete(long id);

    public List<Trening> find(String naziv, Integer tipId, Double minCena, Double maxCena, String treneri, String vrsta, String nivo, String sortiranje, String uredjenost);

}
