package ftn.com.ProjekatTrening.service;

import ftn.com.ProjekatTrening.model.ClanskaKartica;
import ftn.com.ProjekatTrening.model.EStatusClanskeKartice;

import java.util.List;

public interface ClanskaKarticaService {
    void save(ClanskaKartica clanskaKartica);
    List<ClanskaKartica> nadjiPoStatusu(EStatusClanskeKartice status);
    void promeniStatus(long id, EStatusClanskeKartice status);
    boolean postojiPrihvacenIliNaCekanjuZaKorisnika(String korisnickoIme);
    void update(ClanskaKartica clanskaKartica);
    ClanskaKartica findByKorisnik(String korisnickoIme);



}
