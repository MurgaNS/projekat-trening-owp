package ftn.com.ProjekatTrening.service;

import ftn.com.ProjekatTrening.model.SpecijalniDatum;
import ftn.com.ProjekatTrening.model.Trening;

import java.time.LocalDate;

public interface SpecijalniDatumService {
    void save(SpecijalniDatum specijalniDatum);
    SpecijalniDatum findByDatum(LocalDate datum);
    SpecijalniDatum findByDatumAndTrening(LocalDate datum, Trening trening);


}
