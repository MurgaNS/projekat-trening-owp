package ftn.com.ProjekatTrening.service;

import ftn.com.ProjekatTrening.model.Rezervacija;
import ftn.com.ProjekatTrening.model.Trening;

import java.time.LocalDateTime;
import java.util.List;

public interface RezervacijaService {
    List<Rezervacija> findByKorisnik(String korisnickoIme);
    void save(Rezervacija rezervacija);
    Rezervacija findOne(long id);
    void delete(long id);
    int countTerminiTreninga(int terminTreningaId);
    List<Rezervacija> findByKorisnikAndDatum(String korisnickoIme, LocalDateTime datum);
    Rezervacija findByKorisnikAndTerminTreninga(String korisnickoIme, long terminTreningaId);
    boolean existsByKorisnikAndTrening(String korisnickoIme, Trening trening);






}
