package ftn.com.ProjekatTrening.service;

import ftn.com.ProjekatTrening.model.Sala;
import org.springframework.stereotype.Service;

import java.util.List;

public interface SalaService {
    public List<Sala> findAll();

    public Sala findOne(long id);

    public List<Sala> find(String oznaka, String uredjenost);

    /*
    public List<Sala> find(String korisnickoIme, String eMail, String pol, Boolean administrator);

     */

    public void save(Sala sala);

    public void update(Sala sala);

    public void delete(long id);

    public boolean existsRezervacija(long salaId);

}
