package ftn.com.ProjekatTrening.service;

import ftn.com.ProjekatTrening.model.TipTreninga;

import java.util.List;

public interface TipTreningaService {
    public TipTreninga findOne(long id);

    public List<TipTreninga> findAll();

    /*
    public List<TipTreninga> find(String korisnickoIme, String eMail, String pol, Boolean administrator);

     */

    public void save(TipTreninga tipTreninga);

    public void update(TipTreninga tipTreninga);

    public void delete(long id);
}
