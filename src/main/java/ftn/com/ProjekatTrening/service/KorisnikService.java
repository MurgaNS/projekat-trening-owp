package ftn.com.ProjekatTrening.service;


import ftn.com.ProjekatTrening.model.EUloga;
import ftn.com.ProjekatTrening.model.Korisnik;

import java.util.List;

public interface KorisnikService {

	Korisnik findOne(String korisnickoIme);
	Korisnik findOne(String korisnickoIme, String lozinka);
	List<Korisnik> findAll();
	void save(Korisnik korisnik);
	List<Korisnik> save(List<Korisnik> korisnici);
	void update(Korisnik korisnik);
	List<Korisnik> update(List<Korisnik> korisnici);
	void delete(String korisnickoIme);
	void delete(List<String> korisnickaImena);
	List<Korisnik> findByKorisnickoIme(String korisnickoIme);
	List<Korisnik> findByKorisnickoImeAndUloga(String korisnickoIme, EUloga uloga, String sortiranje, String uredjenost);


}
