package ftn.com.ProjekatTrening.service.impl;

import ftn.com.ProjekatTrening.dao.RezervacijaDAO;
import ftn.com.ProjekatTrening.dao.TerminTreningaDAO;
import ftn.com.ProjekatTrening.model.Rezervacija;
import ftn.com.ProjekatTrening.model.Trening;
import ftn.com.ProjekatTrening.service.RezervacijaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class DatabaseRezervacijaService implements RezervacijaService {

    @Autowired
    private RezervacijaDAO rezervacijaDAO;

    @Override
    public List<Rezervacija> findByKorisnik(String korisnickoIme) {
        return rezervacijaDAO.findByKorisnik(korisnickoIme);
    }

    @Override
    public void save(Rezervacija rezervacija) {
        rezervacijaDAO.save(rezervacija);

    }

    @Override
    public Rezervacija findOne(long id) {
        return rezervacijaDAO.findOne(id);
    }

    @Override
    public void delete(long id) {
        rezervacijaDAO.delete(id);
    }

    @Override
    public int countTerminiTreninga(int terminTreningaId) {
        return rezervacijaDAO.countTerminiTreninga(terminTreningaId);
    }

    @Override
    public List<Rezervacija> findByKorisnikAndDatum(String korisnickoIme, LocalDateTime datum) {
        return rezervacijaDAO.findByKorisnikAndDatum(korisnickoIme, datum);
    }

    @Override
    public Rezervacija findByKorisnikAndTerminTreninga(String korisnickoIme, long terminTreningaId) {
        return rezervacijaDAO.findByKorisnikAndTerminTreninga(korisnickoIme, terminTreningaId);
    }

    @Override
    public boolean existsByKorisnikAndTrening(String korisnickoIme, Trening trening) {
        return rezervacijaDAO.existsByKorisnikAndTrening(korisnickoIme, trening);
    }
}
