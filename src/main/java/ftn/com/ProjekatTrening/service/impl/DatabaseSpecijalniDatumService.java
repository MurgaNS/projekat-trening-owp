package ftn.com.ProjekatTrening.service.impl;

import ftn.com.ProjekatTrening.dao.SpecijalniDatumDAO;
import ftn.com.ProjekatTrening.model.SpecijalniDatum;
import ftn.com.ProjekatTrening.model.Trening;
import ftn.com.ProjekatTrening.service.SpecijalniDatumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class DatabaseSpecijalniDatumService implements SpecijalniDatumService {
    @Autowired
    private SpecijalniDatumDAO specijalniDatumDAO;


    @Override
    public void save(SpecijalniDatum specijalniDatum) {
        specijalniDatumDAO.save(specijalniDatum);
    }

    @Override
    public SpecijalniDatum findByDatum(LocalDate datum) {
        return specijalniDatumDAO.findByDatum(datum);
    }

    @Override
    public SpecijalniDatum findByDatumAndTrening(LocalDate datum, Trening trening) {
        return specijalniDatumDAO.findByDatumAndTrening(datum, trening);
    }
}
