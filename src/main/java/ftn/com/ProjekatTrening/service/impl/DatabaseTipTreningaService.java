package ftn.com.ProjekatTrening.service.impl;

import ftn.com.ProjekatTrening.dao.TipTreningaDAO;
import ftn.com.ProjekatTrening.model.TipTreninga;
import ftn.com.ProjekatTrening.service.TipTreningaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DatabaseTipTreningaService implements TipTreningaService {

    @Autowired
    private TipTreningaDAO tipTreningaDAO;

    @Override
    public TipTreninga findOne(long id) {
        return tipTreningaDAO.findOne(id);
    }

    @Override
    public List<TipTreninga> findAll() {
        return tipTreningaDAO.findAll();
    }

    @Override
    public void save(TipTreninga tipTreninga) {
        tipTreningaDAO.save(tipTreninga);
    }

    @Override
    public void update(TipTreninga tipTreninga) {
        tipTreningaDAO.update(tipTreninga);
    }

    @Override
    public void delete(long id) {
        tipTreningaDAO.delete(id);
    }
}
