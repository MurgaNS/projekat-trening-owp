package ftn.com.ProjekatTrening.service.impl;

import ftn.com.ProjekatTrening.dao.TerminTreningaDAO;
import ftn.com.ProjekatTrening.model.Izvestaj;
import ftn.com.ProjekatTrening.model.Sala;
import ftn.com.ProjekatTrening.model.TerminTreninga;
import ftn.com.ProjekatTrening.service.TerminTreningaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class DatabaseTerminTreningaService implements TerminTreningaService {

    @Autowired
    private TerminTreningaDAO terminTreningaDAO;
    @Override
    public TerminTreninga findOne(long id) {
        return terminTreningaDAO.findOne(id);
    }

    @Override
    public List<TerminTreninga> findAll() {
        return null;
    }

    @Override
    public void save(TerminTreninga terminTreninga) {
        terminTreningaDAO.save(terminTreninga);
    }

    @Override
    public void update(TerminTreninga terminTreninga) {

    }

    @Override
    public void delete(long id) {

    }

    @Override
    public List<TerminTreninga> aktuelniTerminiZaTrening(long treningId) {
        return terminTreningaDAO.aktuelniTerminiZaTrening(treningId);
    }

    @Override
    public void rezervacija(int terminTreningaId, String korisnickoIme) {
        terminTreningaDAO.rezervacija(terminTreningaId, korisnickoIme);
    }

    @Override
    public List<Izvestaj> getIzvestaji(LocalDateTime pocetniDatum, LocalDateTime krajnjiDatum) {
        return terminTreningaDAO.getIzvestaji(pocetniDatum, krajnjiDatum);
    }

    @Override
    public TerminTreninga findTerminTreningaBySalaAndDatum(Sala sala, LocalDateTime pocetniDatum, LocalDateTime krajnjiDatum) {
        return terminTreningaDAO.findTerminTreningaBySalaAndDatum(sala, pocetniDatum, krajnjiDatum);
    }
}
