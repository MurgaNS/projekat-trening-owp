package ftn.com.ProjekatTrening.service.impl;

import ftn.com.ProjekatTrening.dao.SalaDAO;
import ftn.com.ProjekatTrening.model.Sala;
import ftn.com.ProjekatTrening.service.SalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DatabaseSalaService implements SalaService {

    @Autowired
    private SalaDAO salaDAO;


    @Override
    public List<Sala> findAll() {
        return salaDAO.findAll();
    }

    @Override
    public Sala findOne(long id) {
        return salaDAO.findOne(id);
    }

    @Override
    public List<Sala> find(String oznaka, String uredjenost) {
        return salaDAO.find(oznaka, uredjenost);
    }

    @Override
    public void save(Sala sala) {
        salaDAO.save(sala);

    }

    @Override
    public void update(Sala sala) {
        salaDAO.update(sala);
    }

    @Override
    public void delete(long id) {
        salaDAO.delete(id);
    }

    @Override
    public boolean existsRezervacija(long salaId) {
        return salaDAO.existsRezervacija(salaId);
    }
}
