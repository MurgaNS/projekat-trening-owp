package ftn.com.ProjekatTrening.service.impl;

import ftn.com.ProjekatTrening.dao.ListaZeljaDAO;
import ftn.com.ProjekatTrening.model.ListaZelja;
import ftn.com.ProjekatTrening.service.ListaZeljaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DatabaseListaZeljaService implements ListaZeljaService {

    @Autowired
    private ListaZeljaDAO listaZeljaDAO;

    @Override
    public List<ListaZelja> findByKorisnik(String korisnickoIme) {
        return listaZeljaDAO.findByKorisnik(korisnickoIme);
    }

    @Override
    public void delete(long id) {
        listaZeljaDAO.delete(id);
    }

    @Override
    public void save(ListaZelja listaZelja) {
        listaZeljaDAO.save(listaZelja);
    }
}
