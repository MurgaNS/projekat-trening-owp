package ftn.com.ProjekatTrening.service.impl;

import ftn.com.ProjekatTrening.dao.KomentarDAO;
import ftn.com.ProjekatTrening.model.EStatusKomentara;
import ftn.com.ProjekatTrening.model.Komentar;
import ftn.com.ProjekatTrening.service.KomentarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DatabaseKomentarService implements KomentarService {

    @Autowired
    private KomentarDAO komentarDAO;


    @Override
    public List<Komentar> findAll() {
        return null;
    }

    @Override
    public void save(Komentar komentar) {
        komentarDAO.save(komentar);
    }

    @Override
    public void update(Komentar komentar) {

    }

    @Override
    public void delete(long id) {

    }

    @Override
    public List<Komentar> nadjiPoTreningId(long treningId) {
        return komentarDAO.nadjiPoTreningId(treningId);
    }

    @Override
    public List<Komentar> nadjiPoStatusu(EStatusKomentara status) {
        return komentarDAO.nadjiPoStatusu(status);
    }

    @Override
    public void promeniStatus(int id, EStatusKomentara status) {
        komentarDAO.promeniStatus(id, status);
    }
}
