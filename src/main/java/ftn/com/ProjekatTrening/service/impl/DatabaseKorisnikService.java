package ftn.com.ProjekatTrening.service.impl;

import ftn.com.ProjekatTrening.dao.KorisnikDAO;
import ftn.com.ProjekatTrening.model.EUloga;
import ftn.com.ProjekatTrening.model.Korisnik;
import ftn.com.ProjekatTrening.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DatabaseKorisnikService implements KorisnikService {

	@Autowired
	private KorisnikDAO korisnikDAO;
	
	@Override
	public Korisnik findOne(String korisnickoIme) {
		return korisnikDAO.findOne(korisnickoIme);
	}

	@Override
	public Korisnik findOne(String korisnickoIme, String lozinka) {
		return korisnikDAO.findOne(korisnickoIme, lozinka);
	}

	@Override
	public List<Korisnik> findAll() {
		return korisnikDAO.findAll();
	}

	@Override
	public void save(Korisnik korisnik) {
		korisnikDAO.save(korisnik);
	}

	@Override
	public List<Korisnik> save(List<Korisnik> korisnici) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Korisnik korisnik) {
		korisnikDAO.update(korisnik);
	}

	@Override
	public List<Korisnik> update(List<Korisnik> korisnici) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(String korisnickoIme) {
		Korisnik korisnik = findOne(korisnickoIme);
		if (korisnik != null) {
			korisnikDAO.delete(korisnickoIme);
		}
	}

	@Override
	public void delete(List<String> korisnickaImena) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<Korisnik> findByKorisnickoIme(String korisnickoIme) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Korisnik> findByKorisnickoImeAndUloga(String korisnickoIme, EUloga uloga, String sortiranje, String uredjenost) {
		return korisnikDAO.findByKorisnickoImeAndUloga(korisnickoIme, uloga, sortiranje, uredjenost);
	}

}
