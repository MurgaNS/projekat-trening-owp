package ftn.com.ProjekatTrening.service.impl;

import ftn.com.ProjekatTrening.dao.ClanskaKarticaDAO;
import ftn.com.ProjekatTrening.model.ClanskaKartica;
import ftn.com.ProjekatTrening.model.EStatusClanskeKartice;
import ftn.com.ProjekatTrening.service.ClanskaKarticaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DatabaseClanskaKarticaService implements ClanskaKarticaService {
    @Autowired
    private ClanskaKarticaDAO clanskaKarticaDAO;

    @Override
    public void save(ClanskaKartica clanskaKartica) {
        clanskaKarticaDAO.save(clanskaKartica);
    }

    @Override
    public List<ClanskaKartica> nadjiPoStatusu(EStatusClanskeKartice status) {
        return clanskaKarticaDAO.nadjiPoStatusu(status);
    }

    @Override
    public void promeniStatus(long id, EStatusClanskeKartice status) {
        clanskaKarticaDAO.promeniStatus(id, status);
    }

    @Override
    public boolean postojiPrihvacenIliNaCekanjuZaKorisnika(String korisnickoIme) {
        return clanskaKarticaDAO.postojiPrihvacenIliNaCekanjuZaKorisnika(korisnickoIme);
    }

    @Override
    public void update(ClanskaKartica clanskaKartica) {
        clanskaKarticaDAO.update(clanskaKartica);
    }

    @Override
    public ClanskaKartica findByKorisnik(String korisnickoIme) {
        return clanskaKarticaDAO.findByKorisnik(korisnickoIme);
    }
}
