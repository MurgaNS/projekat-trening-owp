package ftn.com.ProjekatTrening.service.impl;

import ftn.com.ProjekatTrening.controller.KorisnikController;
import ftn.com.ProjekatTrening.model.Korisnik;
import ftn.com.ProjekatTrening.model.TerminTreninga;
import ftn.com.ProjekatTrening.model.Trening;
import ftn.com.ProjekatTrening.service.KorpaService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service
public class KorpaServiceImpl implements KorpaService {

    @Override
    public List<TerminTreninga> nadjiSve(HttpSession session) {
        Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        return (List<TerminTreninga>) session.getAttribute(korisnik.getKorisnickoIme());

    }

    @Override
    public void dodaj(HttpSession session, TerminTreninga terminTreninga) {
        Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        List<TerminTreninga> termini =  (List<TerminTreninga>) session.getAttribute(korisnik.getKorisnickoIme());
        if(termini == null){
            termini = new ArrayList<>();
        }
        termini.add(terminTreninga);
        session.setAttribute(korisnik.getKorisnickoIme(), termini);

    }

    @Override
    public void obrisi(HttpSession session, long id) {
        Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        List<TerminTreninga> termini =  (List<TerminTreninga>) session.getAttribute(korisnik.getKorisnickoIme());
        TerminTreninga treningZaBrisanje = null;
        for(TerminTreninga trening: termini){
            if(trening.getId() == id){
                treningZaBrisanje = trening;
                break;
            }
        }
        termini.remove(treningZaBrisanje);
        session.setAttribute(korisnik.getKorisnickoIme(), termini);


    }

    @Override
    public boolean postoji(HttpSession session, long id) {
        Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
        List<TerminTreninga> termini =  (List<TerminTreninga>) session.getAttribute(korisnik.getKorisnickoIme());
        if(termini == null){
            return false;
        }
        for(TerminTreninga trening: termini){
            if(trening.getId() == id){
               return true;
            }
        }
        return false;
    }
}
