package ftn.com.ProjekatTrening.service.impl;

import ftn.com.ProjekatTrening.dao.TreningDAO;
import ftn.com.ProjekatTrening.model.Trening;
import ftn.com.ProjekatTrening.service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DatabaseTreningService implements TreningService {

    @Autowired
    private TreningDAO treningDAO;

    @Override
    public Trening findOne(long id) {
        return treningDAO.findOne(id);
    }

    @Override
    public List<Trening> findAll() {
        return treningDAO.findAll();
    }

    @Override
    public void save(Trening trening) {
        treningDAO.save(trening);
    }

    @Override
    public void update(Trening trening) {
        treningDAO.update(trening);
    }

    @Override
    public void delete(long id) {
        treningDAO.delete(id);
    }

    @Override
    public List<Trening> find(String naziv, Integer tipId, Double minCena, Double maxCena, String treneri, String vrsta, String nivo, String sortiranje, String uredjenost) {
        return treningDAO.find(naziv, tipId, minCena, maxCena, treneri, vrsta, nivo, sortiranje, uredjenost);
    }
}
