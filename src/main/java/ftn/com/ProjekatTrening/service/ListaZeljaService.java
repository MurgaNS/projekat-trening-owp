package ftn.com.ProjekatTrening.service;

import ftn.com.ProjekatTrening.model.ListaZelja;

import java.util.List;

public interface ListaZeljaService {

    List<ListaZelja> findByKorisnik(String korisnickoIme);
    void delete(long id);
    void save(ListaZelja listaZelja);
}
