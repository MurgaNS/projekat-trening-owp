package ftn.com.ProjekatTrening.dao;

import ftn.com.ProjekatTrening.model.Sala;

import java.time.LocalDateTime;
import java.util.List;

public interface SalaDAO {

    public Sala findOne(long id);
    public List<Sala> findAll();
    public List<Sala> find(String oznaka, String uredjenost);

    /*
    public List<Sala> find(String korisnickoIme, String eMail, String pol, Boolean administrator);

     */

    public void save(Sala sala);

    public void update(Sala sala);

    public void delete(long id);

    public boolean existsRezervacija(long salaId);


}
