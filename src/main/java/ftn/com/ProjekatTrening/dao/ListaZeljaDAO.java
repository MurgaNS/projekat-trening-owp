package ftn.com.ProjekatTrening.dao;

import ftn.com.ProjekatTrening.model.ListaZelja;

import java.util.List;

public interface ListaZeljaDAO {

    List<ListaZelja> findByKorisnik(String korisnickoIme);
    void delete(long id);
    void save(ListaZelja listaZelja);
}
