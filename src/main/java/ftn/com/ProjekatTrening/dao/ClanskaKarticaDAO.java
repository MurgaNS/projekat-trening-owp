package ftn.com.ProjekatTrening.dao;

import ftn.com.ProjekatTrening.model.ClanskaKartica;
import ftn.com.ProjekatTrening.model.EStatusClanskeKartice;
import ftn.com.ProjekatTrening.model.Korisnik;

import java.util.List;

public interface ClanskaKarticaDAO {
    void save(ClanskaKartica clanskaKartica);
    ClanskaKartica findByKorisnik(String korisnickoIme);
    List<ClanskaKartica> nadjiPoStatusu(EStatusClanskeKartice status);
    void promeniStatus(long id, EStatusClanskeKartice status);
    boolean postojiPrihvacenIliNaCekanjuZaKorisnika(String korisnickoIme);
    void update(ClanskaKartica clanskaKartica);
}
