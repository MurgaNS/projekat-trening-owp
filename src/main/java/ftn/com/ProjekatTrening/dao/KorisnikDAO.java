package ftn.com.ProjekatTrening.dao;

import ftn.com.ProjekatTrening.model.EUloga;
import ftn.com.ProjekatTrening.model.Korisnik;

import java.util.List;

public interface KorisnikDAO {
    public Korisnik findOne(String korisnickoIme);

    public Korisnik findOne(String korisnickoIme, String lozinka);

    public List<Korisnik> findAll();

    /*
    public List<Korisnik> find(String korisnickoIme, String eMail, String pol, Boolean administrator);

     */

    public void save(Korisnik korisnik);

    public void update(Korisnik korisnik);

    public void delete(String korisnickoIme);

    public List<Korisnik> findByKorisnickoImeAndUloga(String korisnickoIme, EUloga uloga, String sortiranje, String uredjenost);
}
