package ftn.com.ProjekatTrening.dao;

import ftn.com.ProjekatTrening.model.SpecijalniDatum;
import ftn.com.ProjekatTrening.model.Trening;

import java.time.LocalDate;

public interface SpecijalniDatumDAO {

    void save(SpecijalniDatum specijalniDatum);
    SpecijalniDatum findByDatum(LocalDate datum);
    SpecijalniDatum findByDatumAndTrening(LocalDate datum, Trening trening);
}
