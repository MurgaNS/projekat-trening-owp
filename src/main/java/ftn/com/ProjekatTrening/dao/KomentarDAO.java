package ftn.com.ProjekatTrening.dao;

import ftn.com.ProjekatTrening.model.EStatusKomentara;
import ftn.com.ProjekatTrening.model.Komentar;

import java.util.List;

public interface KomentarDAO { public Komentar findOne(long id);

    public List<Komentar> findAll();

    /*
    public List<Komentar> find(String korisnickoIme, String eMail, String pol, Boolean administrator);

     */

    public void save(Komentar komentar);

    public void update(Komentar komentar);

    public void delete(long id);

    public List<Komentar> nadjiPoTreningId(long treningId);

    public List<Komentar> nadjiPoStatusu(EStatusKomentara status);

    public void promeniStatus(int id, EStatusKomentara status);


}
