package ftn.com.ProjekatTrening.dao.impl;

import ftn.com.ProjekatTrening.dao.KorisnikDAO;
import ftn.com.ProjekatTrening.dao.ListaZeljaDAO;
import ftn.com.ProjekatTrening.dao.TreningDAO;
import ftn.com.ProjekatTrening.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class ListaZeljaDAOImpl implements ListaZeljaDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private KorisnikDAO korisnikDAO;
    @Autowired
    private TreningDAO treningDAO;

    private class ListaZeljaRowMapper implements RowMapper<ListaZelja> {
        @Override
        public ListaZelja mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            long id = rs.getLong(index++);
            long treningId = rs.getLong(index++);
            String korisnickoIme = rs.getString(index++);
            Trening trening = treningDAO.findOne(treningId);
            Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
            ListaZelja zelja = new ListaZelja(id, trening, korisnik);
            return zelja;
        }

    }

    @Override
    public List<ListaZelja> findByKorisnik(String korisnickoIme) {
        String sql = "SELECT * FROM listaZelja WHERE korisnickoIme=?";
        return jdbcTemplate.query(sql, new ListaZeljaRowMapper(), korisnickoIme);
    }

    @Override
    public void delete(long id) {
        String sql = "DELETE FROM listaZelja WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public void save(ListaZelja listaZelja) {
        String sql = "INSERT INTO listaZelja (treningId, korisnickoIme) VALUES (?, ?)";
        jdbcTemplate.update(sql, listaZelja.getTrening().getId(), listaZelja.getKorisnik().getKorisnickoIme());
    }
}
