package ftn.com.ProjekatTrening.dao.impl;

import ftn.com.ProjekatTrening.dao.SalaDAO;
import ftn.com.ProjekatTrening.model.EUloga;
import ftn.com.ProjekatTrening.model.Korisnik;
import ftn.com.ProjekatTrening.model.Sala;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class SalaDAOImpl implements SalaDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class SalaRowMapper implements RowMapper<Sala> {
        @Override
        public Sala mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            long id = rs.getLong(index++);
            String oznaka = rs.getString(index++);

            int kapacitet = rs.getInt(index++);
            Sala sala = new Sala(id, oznaka, kapacitet);


            return sala;
        }

    }

    @Override
    public Sala findOne(long id) {
        try {
            String sql = "SELECT * FROM sala WHERE id = ?";
            return jdbcTemplate.queryForObject(sql, new SalaDAOImpl.SalaRowMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            // ako tipTreninga nije pronađen
            return null;
        }
    }

    @Override
    public List<Sala> findAll() {
        String sql = "SELECT * FROM sala";
        return jdbcTemplate.query(sql, new SalaDAOImpl.SalaRowMapper());
    }

    @Override
    public List<Sala> find(String oznaka, String uredjenost) {
        oznaka = "%" + oznaka + "%";
        String sql = "SELECT * from sala WHERE oznaka LIKE ? ORDER BY oznaka ";
        if(uredjenost.equals("Rastuce")){
            sql += "ASC";
        }
        else{
            sql += "DESC";
        }

        return jdbcTemplate.query(sql, new SalaRowMapper(), oznaka);
    }

    @Override
    public void save(Sala sala) {
        String sql = "INSERT INTO sala (oznaka, kapacitet) VALUES (?, ?)";
        jdbcTemplate.update(sql, sala.getOznaka(), sala.getKapacitet());

    }

    @Override
    public void update(Sala sala) {
        String sql = "UPDATE sala SET kapacitet=? WHERE id=?";
        jdbcTemplate.update(sql, sala.getKapacitet(), sala.getId());
    }

    @Override
    public void delete(long id) {
        String sql = "DELETE FROM sala WHERE id = ?";
        jdbcTemplate.update(sql, id);

    }

    @Override
    public boolean existsRezervacija(long salaId) {
        String sql = "SELECT count(*) FROM rezervacija inner join terminTreninga inner join sala on rezervacija.terminTreningaId=terminTreninga.id and sala.id=terminTreninga.salaId WHERE salaId = ?";

        int count = jdbcTemplate.queryForObject(sql, new Object[] { salaId }, Integer.class);

        return count > 0;
    }
}
