package ftn.com.ProjekatTrening.dao.impl;

import ftn.com.ProjekatTrening.dao.SpecijalniDatumDAO;
import ftn.com.ProjekatTrening.dao.TreningDAO;
import ftn.com.ProjekatTrening.model.Sala;
import ftn.com.ProjekatTrening.model.SpecijalniDatum;
import ftn.com.ProjekatTrening.model.Trening;
import ftn.com.ProjekatTrening.service.TreningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

@Repository
public class SpecijalniDatumDAOImpl implements SpecijalniDatumDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TreningDAO treningDAO;


    private class SpecijalniDatumRowMapper implements RowMapper<SpecijalniDatum> {
        @Override
        public SpecijalniDatum mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            long id = rs.getLong(index++);
            LocalDate datum = rs.getDate(index++).toLocalDate();
            int popust = rs.getInt(index++);
            Integer treningId = rs.getInt(index++);
            Trening trening = treningDAO.findOne(treningId);
            SpecijalniDatum specijalniDatum = new SpecijalniDatum(id, datum, popust, trening);
            return specijalniDatum;
        }

    }

    @Override
    public void save(SpecijalniDatum specijalniDatum) {
        String sql = "INSERT INTO specijalniDatum (datum, popust, treningId) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, specijalniDatum.getDatum(), specijalniDatum.getPopust(), specijalniDatum.getTrening() != null ? specijalniDatum.getTrening().getId() : null  );
    }

    @Override
    public SpecijalniDatum findByDatum(LocalDate datum) {
        try {
            String sql = "SELECT * FROM specijalniDatum WHERE datum=? and treningId = NULL";
            return jdbcTemplate.queryForObject(sql, new SpecijalniDatumRowMapper(), datum);
        }
        catch(EmptyResultDataAccessException exc){
            return null;
        }
    }

    @Override
    public SpecijalniDatum findByDatumAndTrening(LocalDate datum, Trening trening) {
        try {
            String sql = "SELECT * FROM specijalniDatum WHERE datum=? and treningId=?";
            return jdbcTemplate.queryForObject(sql, new SpecijalniDatumRowMapper(), datum, trening.getId());
        }
        catch(EmptyResultDataAccessException exc){
            return null;
        }
    }


}
