package ftn.com.ProjekatTrening.dao.impl;

import ftn.com.ProjekatTrening.dao.TipTreningaDAO;
import ftn.com.ProjekatTrening.dao.TreningDAO;
import ftn.com.ProjekatTrening.model.ENivoTreninga;
import ftn.com.ProjekatTrening.model.EVrstaTreninga;
import ftn.com.ProjekatTrening.model.TipTreninga;
import ftn.com.ProjekatTrening.model.Trening;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TreningDAOImpl implements TreningDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TipTreningaDAO tipTreningaDAO;


    private class TreningRowMapper implements RowMapper<Trening> {
        @Override
        public Trening mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            long id = rs.getLong(index++);
            String naziv = rs.getString(index++);
            String treneri = rs.getString(index++);
            String opis = rs.getString(index++);
            String slika = rs.getString(index++);
            long tipId = rs.getLong(index++);
            TipTreninga tip = tipTreningaDAO.findOne(tipId);
            double cena = rs.getDouble(index++);
            EVrstaTreninga vrsta = EVrstaTreninga.valueOf(rs.getString(index++));
            ENivoTreninga nivo = ENivoTreninga.valueOf(rs.getString(index++));
            int trajanje = rs.getInt(index++);
            double prosecnaOcena = rs.getDouble(index++);

            Trening trening = new Trening(id, naziv, treneri, opis, slika, tip, cena, vrsta, nivo, trajanje);

            trening.setProsecnaOcena(prosecnaOcena);

            return trening;
        }

    }
    @Override
    public Trening findOne(long id) {
        try {
            String sql = "select trening.*, avg(komentar.ocena) as prosecnaOcena from trening left join komentar on trening.id = komentar.treningId and  komentar.status = 'ODOBREN' group by trening.id having id = ?";
            return jdbcTemplate.queryForObject(sql, new TreningRowMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            // ako trening nije pronađen
            return null;
        }
    }

    @Override
    public List<Trening> findAll() {
        String sql = "select trening.*, avg(komentar.ocena) as prosecnaOcena from trening left join komentar on trening.id = komentar.treningId and komentar.status = 'ODOBREN' group by trening.id ";
        return jdbcTemplate.query(sql, new TreningRowMapper());
    }


    @Override
    public void save(Trening trening) {
        String sql = "INSERT INTO trening (naziv, treneri, opis, slika, tipId, cena, vrsta, nivo, trajanje) VALUES (?, ?, ?, ?, ?, ?,?,?,?)";
        jdbcTemplate.update(sql, trening.getNaziv(), trening.getTreneri(), trening.getOpis(),
                trening.getSlika(), trening.getTip().getId(), trening.getCena(), trening.getVrsta().toString(),
                trening.getNivo().toString(), trening.getTrajanje());
    }
    @Override
    public void update(Trening trening) {
        String sql = "UPDATE trening SET naziv=?, treneri=?, opis=?, slika=?, tipId=?, cena=?, vrsta=?, nivo=?, trajanje=? WHERE id=?";
        jdbcTemplate.update(sql, trening.getNaziv(), trening.getTreneri(), trening.getOpis(),
                trening.getSlika(), trening.getTip().getId(), trening.getCena(), trening.getVrsta().toString(),
                trening.getNivo().toString(), trening.getTrajanje(), trening.getId());

    }
    @Override
    public void delete(long id) {
        String sql = "DELETE FROM trening WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public List<Trening> find(String naziv, Integer tipId, Double minCena, Double maxCena, String treneri, String vrsta, String nivo, String sortiranje, String uredjenost) {
        String sql = "select trening.*, avg(komentar.ocena) as prosecnaOcena from trening left join komentar on trening.id = komentar.treningId and komentar.status = 'ODOBREN' group by trening.id";
       // String sql = "SELECT * FROM trening LEFT JOIN tipTreninga ON trening.tipId = tipTreninga.id";
        List<Object> listaArgumenata = new ArrayList<>();
        StringBuffer whereSql = new StringBuffer(" HAVING ");
        StringBuffer orderSql = new StringBuffer(" ");
        boolean imaArgumenata = false;
        if(!naziv.isBlank()) {
            naziv = "%" + naziv + "%";
            if (imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("naziv LIKE ? ");
            imaArgumenata = true;
            listaArgumenata.add(naziv);
        }
        if(tipId!=-1) {

            if (imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("tipId = ? ");
            imaArgumenata = true;
            listaArgumenata.add(tipId);
        }
        if(minCena!=-1) {

            if (imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("cena >= ? ");
            imaArgumenata = true;
            listaArgumenata.add(minCena);
        }
        if(maxCena!=-1) {

            if (imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("cena <= ? ");
            imaArgumenata = true;
            listaArgumenata.add(maxCena);
        }
        if(!treneri.isBlank()) {
            treneri = "%" + treneri + "%";
            if (imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("treneri LIKE ? ");
            imaArgumenata = true;
            listaArgumenata.add(treneri);
        }
        if(!vrsta.isBlank()) {
            if (imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("vrsta = ? ");
            imaArgumenata = true;
            listaArgumenata.add(vrsta);
        }
        if(!nivo.isBlank()) {
            if (imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("nivo = ? ");
            imaArgumenata = true;
            listaArgumenata.add(nivo);
        }
       if(sortiranje.equals("Naziv")){
           orderSql.append(" ORDER BY naziv ");
       }
       else if(sortiranje.equals("Cena")){
           orderSql.append(" ORDER BY cena ");

       }
       else if(sortiranje.equals("Treneri")){
           orderSql.append(" ORDER BY treneri ");

       }
       else if(sortiranje.equals("Vrsta")){
           orderSql.append(" ORDER BY vrsta ");

       }
       else if(sortiranje.equals("Nivo")){
           orderSql.append(" ORDER BY nivo ");

       }
       else if(sortiranje.equals("Tip")){
           orderSql.append(" ORDER BY ime ");

       }
       else if(sortiranje.equals("Prosecna ocena")){
           orderSql.append(" ORDER BY prosecnaOcena ");

       }
       if(!sortiranje.isBlank()) {
           if (uredjenost.equals("Opadajuce")) {
               orderSql.append(" DESC ");
           } else if (uredjenost.equals("Rastuce")) {
               orderSql.append(" ASC ");
           }
       }


    if(imaArgumenata) {
        sql = sql + whereSql.toString();
    }
    if(orderSql.length() != 1){
        sql = sql +  orderSql.toString();
    }

        System.out.println(sql);




        return jdbcTemplate.query(sql, listaArgumenata.toArray(), new TreningRowMapper());
    }

}
