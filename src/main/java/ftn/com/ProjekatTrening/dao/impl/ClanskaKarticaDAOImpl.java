package ftn.com.ProjekatTrening.dao.impl;

import ftn.com.ProjekatTrening.dao.ClanskaKarticaDAO;
import ftn.com.ProjekatTrening.dao.KorisnikDAO;
import ftn.com.ProjekatTrening.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@Repository
public class ClanskaKarticaDAOImpl implements ClanskaKarticaDAO {


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KorisnikDAO korisnikDAO;

    private class ClanskaKarticaRowMapper implements RowMapper<ClanskaKartica> {
        @Override
        public ClanskaKartica mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            long id = rs.getLong(index++);
            int popust = rs.getInt(index++);
            int brojPoena = rs.getInt(index++);
            EStatusClanskeKartice status = EStatusClanskeKartice.valueOf(rs.getString(index++));
            String korisnickoIme = rs.getString(index++);
            Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
            ClanskaKartica clanskaKartica = new ClanskaKartica(id, popust, brojPoena, status, korisnik);
            return clanskaKartica;




        }

    }
    @Override
    public ClanskaKartica findByKorisnik(String korisnickoIme) {
        try {
            String sql = "SELECT * FROM clanskaKartica WHERE korisnickoIme = ?";
            return jdbcTemplate.queryForObject(sql, new ClanskaKarticaRowMapper(), korisnickoIme);
        } catch (EmptyResultDataAccessException ex) {
            // ako korisnik nije pronađen
            return null;
        }
    }
    @Override
    public void save(ClanskaKartica clanskaKartica) {
        String sql = "INSERT INTO clanskaKartica(popust, brojPoena, status, korisnickoIme) VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(sql, clanskaKartica.getPopust(), clanskaKartica.getBrojPoena(),clanskaKartica.getStatus().toString(), clanskaKartica.getKorisnik().getKorisnickoIme());

    }

    @Override
    public void update(ClanskaKartica clanskaKartica) {

        String sql = "UPDATE clanskaKartica SET popust=?, brojPoena=?, status=?  WHERE id = ?";
        jdbcTemplate.update(sql, clanskaKartica.getPopust(), clanskaKartica.getBrojPoena(), clanskaKartica.getStatus().toString(), clanskaKartica.getId());

    }

    @Override
    public List<ClanskaKartica> nadjiPoStatusu(EStatusClanskeKartice status) {
        String sql = "SELECT * FROM clanskaKartica WHERE status=?";
        return jdbcTemplate.query(sql, new ClanskaKarticaRowMapper(), status.toString());
    }

    @Override
    public void promeniStatus(long id, EStatusClanskeKartice status) {
        String sql = "UPDATE clanskaKartica SET status=? WHERE id=?";
        jdbcTemplate.update(sql, status.toString(), id);
    }

    @Override
    public boolean postojiPrihvacenIliNaCekanjuZaKorisnika(String korisnickoIme) {
        try {
            String sql = "SELECT * FROM clanskaKartica WHERE status IN ('NA_CEKANJU', 'ODOBREN') AND korisnickoIme = ?";
            return jdbcTemplate.queryForObject(sql, new ClanskaKarticaRowMapper(), korisnickoIme) != null;
        }
        catch(EmptyResultDataAccessException exc){
            return false;
        }
    }


}
