package ftn.com.ProjekatTrening.dao.impl;

import ftn.com.ProjekatTrening.dao.KorisnikDAO;
import ftn.com.ProjekatTrening.dao.RezervacijaDAO;
import ftn.com.ProjekatTrening.dao.TerminTreningaDAO;
import ftn.com.ProjekatTrening.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class RezervacijaDAOImpl implements RezervacijaDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TerminTreningaDAO terminTreningaDAO;

    @Autowired
    private KorisnikDAO korisnikDAO;
    private class RezervacijaRowMapper implements RowMapper<Rezervacija> {
        @Override
        public Rezervacija mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            long id = rs.getLong(index++);
            long terminTreningaId = rs.getLong(index++);
            String korisnickoIme = rs.getString(index++);
            LocalDateTime datumVreme = rs.getTimestamp(index++).toLocalDateTime();
           TerminTreninga termin = terminTreningaDAO.findOne(terminTreningaId);
            Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
            double cena = rs.getDouble(index++);
            Rezervacija rezervacija = new Rezervacija(id, termin, korisnik, datumVreme, cena);
            return rezervacija;

        }

    }
    @Override
    public List<Rezervacija> findByKorisnik(String korisnickoIme) {
        String sql = "SELECT * FROM rezervacija WHERE korisnickoIme=? ORDER BY datumVreme DESC";
        return jdbcTemplate.query(sql, new RezervacijaRowMapper(), korisnickoIme);
    }

    @Override
    public void save(Rezervacija rezervacija) {
        String sql = "INSERT INTO rezervacija (terminTreningaId, korisnickoIme, datumVreme, cena) VALUES (?, ?, NOW(), ?)";
        jdbcTemplate.update(sql, rezervacija.getTerminTreninga().getId(), rezervacija.getKorisnik().getKorisnickoIme(), rezervacija.getCena());
    }

    @Override
    public Rezervacija findOne(long id) {
        try {
            String sql = "SELECT * FROM rezervacija WHERE id = ?";
            return jdbcTemplate.queryForObject(sql, new RezervacijaRowMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            // ako tipTreninga nije pronađen
            return null;
        }
    }

    @Override
    public void delete(long id) {
        String sql = "DELETE FROM rezervacija WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public int countTerminiTreninga(int terminTreningaId) {
        String sql = "SELECT count(*) FROM rezervacija WHERE terminTreningaId = ?";

        int count = jdbcTemplate.queryForObject(sql, new Object[] { terminTreningaId }, Integer.class);

        return count;
    }

    @Override
    public List<Rezervacija> findByKorisnikAndDatum(String korisnickoIme, LocalDateTime datum) {
        try {
            String sql = "SELECT * FROM rezervacija INNER JOIN terminTreninga ON rezervacija.terminTreningaId = terminTreninga.id WHERE rezervacija.korisnickoIme=? AND terminTreninga.datum = ?";
            System.out.println(sql);
            System.out.println(datum);
            return jdbcTemplate.query(sql, new RezervacijaRowMapper(), korisnickoIme, datum);
        } catch (EmptyResultDataAccessException ex) {
            // ako tipTreninga nije pronađen
            return null;
        }    }

    @Override
    public Rezervacija findByKorisnikAndTerminTreninga(String korisnickoIme, long terminTreningaId) {
        try {
            String sql = "SELECT * FROM rezervacija WHERE korisnickoIme = ? AND terminTreningaId=?";
            return jdbcTemplate.queryForObject(sql, new RezervacijaRowMapper(), korisnickoIme, terminTreningaId);
        } catch (EmptyResultDataAccessException ex) {
            // ako tipTreninga nije pronađen
            return null;
        }
    }

    @Override
    public boolean existsByKorisnikAndTrening(String korisnickoIme, Trening trening) {
        String sql = "SELECT count(*) FROM rezervacija INNER JOIN terminTreninga INNER JOIN trening ON rezervacija.terminTreningaId = terminTreninga.id and terminTreninga.treningId = trening.id WHERE rezervacija.korisnickoIme = ? and trening.id = ?";

        int count = jdbcTemplate.queryForObject(sql, new Object[] { korisnickoIme, trening.getId() }, Integer.class);

        return count > 0;
    }
}
