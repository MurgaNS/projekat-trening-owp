package ftn.com.ProjekatTrening.dao.impl;

import ftn.com.ProjekatTrening.dao.KorisnikDAO;
import ftn.com.ProjekatTrening.model.EUloga;
import ftn.com.ProjekatTrening.model.Korisnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class KorisnikDAOImpl implements KorisnikDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class KorisnikRowMapper implements RowMapper<Korisnik> {
        @Override
        public Korisnik mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            String korisnickoIme = rs.getString(index++);
            String lozinka = rs.getString(index++);
            String email = rs.getString(index++);
            String ime = rs.getString(index++);
            String prezime = rs.getString(index++);
            LocalDate datumRodjenja = rs.getDate(index++).toLocalDate();
            String adresa = rs.getString(index++);
            String brojTelefona = rs.getString(index++);
            LocalDateTime datumVremeRegistracije = rs.getTimestamp(index++).toLocalDateTime();
            EUloga uloga = EUloga.valueOf(rs.getString(index++));
            boolean blokiran = rs.getBoolean(index++);



            Korisnik korisnik = new Korisnik(korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumVremeRegistracije, uloga, blokiran);
            return korisnik;
        }

    }
    @Override
    public Korisnik findOne(String korisnickoIme) {
        try {
            String sql = "SELECT * FROM korisnik WHERE korisnickoIme = ?";
            return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme);
        } catch (EmptyResultDataAccessException ex) {
            // ako korisnik nije pronađen
            return null;
        }
    }
    @Override
    public Korisnik findOne(String korisnickoIme, String lozinka) {
        try {
            String sql = "SELECT * FROM korisnik WHERE korisnickoIme = ? AND lozinka = ?";
            return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme, lozinka);
        } catch (EmptyResultDataAccessException ex) {
            // ako korisnik nije pronađen
            return null;
        }
    }
    @Override
    public List<Korisnik> findAll() {
        String sql = "SELECT * FROM korisnik";
        return jdbcTemplate.query(sql, new KorisnikRowMapper());
    }

    @Override
    public void save(Korisnik korisnik) {
        String sql = "INSERT INTO korisnik (korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumVremeRegistracije, blokiran) VALUES (?, ?, ?, ?,?,?,?,?,NOW(), ?)";
        jdbcTemplate.update(sql, korisnik.getKorisnickoIme(), korisnik.getLozinka(), korisnik.getEmail(),
                korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(),
                korisnik.getBrojTelefona(), korisnik.isBlokiran());
    }
    @Override
    public void update(Korisnik korisnik) {
        if (korisnik.getLozinka() == null) {
            String sql = "UPDATE korisnik SET email=?, ime=?, prezime=?, datumRodjenja=?, adresa=?, brojTelefona=?, blokiran=?, uloga=? WHERE korisnickoIme = ?";
            jdbcTemplate.update(sql, korisnik.getEmail(), korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(), korisnik.getBrojTelefona(), korisnik.isBlokiran(), korisnik.getUloga().toString(), korisnik.getKorisnickoIme());
        } else {
            String sql = "UPDATE korisnik SET lozinka = ?, email=?, ime=?, prezime=?, datumRodjenja=?, adresa=?, brojTelefona=?, blokiran=?, uloga=? WHERE korisnickoIme = ?";
            jdbcTemplate.update(sql, korisnik.getLozinka(), korisnik.getEmail(), korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(), korisnik.getBrojTelefona(), korisnik.isBlokiran(), korisnik.getUloga().toString(), korisnik.getKorisnickoIme());
        }
    }
    @Override
    public void delete(String korisnickoIme) {
        String sql = "DELETE FROM korisnik WHERE korisnickoIme = ?";
        jdbcTemplate.update(sql, korisnickoIme);
    }

    @Override
    public List<Korisnik> findByKorisnickoImeAndUloga(String korisnickoIme, EUloga uloga, String sortiranje, String uredjenost) {
        String sql = "SELECT * FROM korisnik";
        List<Object> listaArgumenata = new ArrayList<>();
        StringBuffer whereSql = new StringBuffer(" WHERE ");
        StringBuffer orderSql = new StringBuffer(" ");
        boolean imaArgumenata = false;
        if(!korisnickoIme.isBlank()) {
            korisnickoIme = "%" + korisnickoIme + "%";
            if (imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("korisnickoIme LIKE ? ");
            imaArgumenata = true;
            listaArgumenata.add(korisnickoIme);
        }
        if(uloga != null) {

            if (imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("uloga = ? ");
            imaArgumenata = true;
            listaArgumenata.add(uloga.toString());
        }
        if(sortiranje.equals("Korisnicko ime")){
            orderSql.append(" ORDER BY korisnickoIme ");
        }
        else if(sortiranje.equals("Uloga")){
            orderSql.append(" ORDER BY uloga ");

        }
        if(!sortiranje.isBlank()) {
            if (uredjenost.equals("Opadajuce")) {
                orderSql.append(" DESC ");
            } else if (uredjenost.equals("Rastuce")) {
                orderSql.append(" ASC ");
            }
        }


        if(imaArgumenata) {
            sql = sql + whereSql.toString();
        }
        if(orderSql.length() != 1){
            sql = sql +  orderSql.toString();
        }

        System.out.println(sql);




        return jdbcTemplate.query(sql, listaArgumenata.toArray(), new KorisnikRowMapper());
    }

}
