package ftn.com.ProjekatTrening.dao.impl;

import ftn.com.ProjekatTrening.dao.KomentarDAO;
import ftn.com.ProjekatTrening.dao.KorisnikDAO;
import ftn.com.ProjekatTrening.dao.SalaDAO;
import ftn.com.ProjekatTrening.dao.TreningDAO;
import ftn.com.ProjekatTrening.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@Repository
public class KomentarDAOImpl implements KomentarDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private KorisnikDAO korisnikDAO;

    @Autowired
    private TreningDAO treningDAO;

    private class KomentarRowMapper implements RowMapper<Komentar> {
        @Override
        public Komentar mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            long id = rs.getLong(index++);
            String tekst = rs.getString(index++);
            int ocena = rs.getInt(index++);
            LocalDate datum = rs.getDate(index++).toLocalDate();
            String korisnickoIme = rs.getString(index++);
            Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
            int treningId = rs.getInt(index++);
            Trening trening = treningDAO.findOne(treningId);
            String statusString = rs.getString(index++);
            EStatusKomentara status = EStatusKomentara.valueOf(statusString);
            boolean anoniman = rs.getBoolean(index++);
            Komentar komentar = new Komentar(id, tekst, ocena, datum, korisnik, trening, status, anoniman);
            return komentar;




        }

    }
    @Override
    public Komentar findOne(long id) {
        return null;
    }

    @Override
    public List<Komentar> findAll() {
        return null;
    }

    @Override
    public void save(Komentar komentar) {
        String sql = "INSERT INTO komentar(tekst, ocena, datum, korisnickoIme, treningId, status, anoniman) VALUES (?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, komentar.getTekst(), komentar.getOcena(), komentar.getDatum(), komentar.getKorisnik().getKorisnickoIme(), komentar.getTrening().getId(), komentar.getStatus().toString(), komentar.isAnoniman());
    }

    @Override
    public void update(Komentar komentar) {

    }

    @Override
    public void delete(long id) {

    }

    @Override
    public List<Komentar> nadjiPoTreningId(long treningId) {
        String sql = "SELECT * FROM komentar WHERE treningId=? AND status='ODOBREN'";
        return jdbcTemplate.query(sql, new KomentarDAOImpl.KomentarRowMapper(), treningId);
    }

    @Override
    public List<Komentar> nadjiPoStatusu(EStatusKomentara status) {
        String sql = "SELECT * FROM komentar WHERE status=?";
        return jdbcTemplate.query(sql, new KomentarDAOImpl.KomentarRowMapper(), status.toString());
    }

    public void promeniStatus(int id, EStatusKomentara status){
        String sql = "UPDATE komentar SET status=? WHERE id=?";
        jdbcTemplate.update(sql, status.toString(), id);
    }
}
