package ftn.com.ProjekatTrening.dao.impl;

import ftn.com.ProjekatTrening.dao.TipTreningaDAO;
import ftn.com.ProjekatTrening.model.EUloga;
import ftn.com.ProjekatTrening.model.TipTreninga;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class TipTreningaDAOImpl implements TipTreningaDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class TipTreningaRowMapper implements RowMapper<TipTreninga> {
        @Override
        public TipTreninga mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            long id = rs.getLong(index++);
            String ime = rs.getString(index++);
            String opis = rs.getString(index++);




            TipTreninga tipTreninga = new TipTreninga(id, ime, opis);
            return tipTreninga;
        }

    }
    @Override
    public TipTreninga findOne(long id) {
        try {
            String sql = "SELECT * FROM tipTreninga WHERE id = ?";
            return jdbcTemplate.queryForObject(sql, new TipTreningaRowMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            // ako tipTreninga nije pronađen
            return null;
        }
    }

    @Override
    public List<TipTreninga> findAll() {
        String sql = "SELECT * FROM tipTreninga";
        return jdbcTemplate.query(sql, new TipTreningaRowMapper());
    }
    /*
    @Override
    public List<TipTreninga> find(String korisnickoIme, String eMail, String pol, Boolean administrator) {

        ArrayList<Object> listaArgumenata = new ArrayList<Object>();

        String sql = "SELECT korisnickoIme, eMail, pol, administrator FROM korisnici ";

        StringBuffer whereSql = new StringBuffer(" WHERE ");
        boolean imaArgumenata = false;

        if(korisnickoIme!=null) {
            korisnickoIme = "%" + korisnickoIme + "%";
            if(imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("korisnickoIme LIKE ?");
            imaArgumenata = true;
            listaArgumenata.add(korisnickoIme);
        }

        if(eMail!=null) {
            eMail = "%" + eMail + "%";
            if(imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("eMail LIKE ?");
            imaArgumenata = true;
            listaArgumenata.add(eMail);
        }

        if(pol!=null) {
            pol = "%" + pol + "%";
            if(imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append("pol LIKE ?");
            imaArgumenata = true;
            listaArgumenata.add(pol);
        }

        if(administrator!=null) {
            //vraća samo administratore ili sve tipTreningae sistema
            String administratorSql = (administrator)? "administrator = 1": "administrator >= 0";
            if(imaArgumenata)
                whereSql.append(" AND ");
            whereSql.append(administratorSql);
            imaArgumenata = true;
        }


        if(imaArgumenata)
            sql=sql + whereSql.toString()+" ORDER BY korisnickoIme";
        else
            sql=sql + " ORDER BY korisnickoIme";
        System.out.println(sql);

        return jdbcTemplate.query(sql, listaArgumenata.toArray(), new TipTreningaRowMapper());
    }
    */

    @Override
    public void save(TipTreninga tipTreninga) {
        String sql = "INSERT INTO tipTreninga (ime, opis) VALUES (?, ?)";
        jdbcTemplate.update(sql, tipTreninga.getIme(), tipTreninga.getOpis());
    }
    @Override
    public void update(TipTreninga tipTreninga) {
        String sql = "UPDATE tipTreninga SET ime=?, opis=? WHERE id=?";
        jdbcTemplate.update(sql, tipTreninga.getIme(), tipTreninga.getOpis(), tipTreninga.getId());

    }
    @Override
    public void delete(long id) {
        String sql = "DELETE FROM tipTreninga WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

}
