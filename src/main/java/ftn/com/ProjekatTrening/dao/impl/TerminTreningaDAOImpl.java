package ftn.com.ProjekatTrening.dao.impl;

import ftn.com.ProjekatTrening.dao.SalaDAO;
import ftn.com.ProjekatTrening.dao.TerminTreningaDAO;
import ftn.com.ProjekatTrening.dao.TreningDAO;
import ftn.com.ProjekatTrening.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Repository
public class TerminTreningaDAOImpl implements TerminTreningaDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;


     @Autowired
     private SalaDAO salaDAO;

     @Autowired
     private TreningDAO treningDAO;

    private class TerminTreningaRowMapper implements RowMapper<TerminTreninga> {
        @Override
        public TerminTreninga mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            long id = rs.getLong(index++);
            long salaId = rs.getLong(index++);
            long treningId = rs.getLong(index++);
            Sala sala = salaDAO.findOne(salaId);
            Trening trening = treningDAO.findOne(treningId);
            LocalDateTime datum = rs.getTimestamp(index++).toInstant()
                    .atZone(ZoneId.of("UTC"))
                    .toLocalDateTime();
            TerminTreninga termin = new TerminTreninga(id, sala, trening, datum);
            return termin;




        }

    }

    private class IzvestajRowMapper implements RowMapper<Izvestaj> {
        @Override
        public Izvestaj mapRow(ResultSet rs, int rowNum) throws SQLException {
            int index = 1;
            long treningId = rs.getLong(index++);
            int brojZakazanihTreninga = rs.getInt(index++);
            double ukupnaCena = rs.getDouble(index++);
            Trening trening = treningDAO.findOne(treningId);
            Izvestaj izvestaj = new Izvestaj(trening, brojZakazanihTreninga, ukupnaCena);
            return izvestaj;




        }

    }
    @Override
    public TerminTreninga findOne(long id) {
        try {
            String sql = "SELECT * FROM terminTreninga WHERE id = ?";
            return jdbcTemplate.queryForObject(sql, new TerminTreningaDAOImpl.TerminTreningaRowMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            // ako tipTreninga nije pronađen
            return null;
        }
    }

    @Override
    public List<TerminTreninga> findAll() {
        return null;
    }

    @Override
    public void save(TerminTreninga terminTreninga) {
        String sql = "INSERT INTO terminTreninga (salaId, treningId, datum) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, terminTreninga.getSala().getId(), terminTreninga.getTrening().getId(), terminTreninga.getDatum());
    }

    @Override
    public void update(TerminTreninga terminTreninga) {

    }

    @Override
    public void delete(long id) {

    }

    @Override
    public List<TerminTreninga> aktuelniTerminiZaTrening(long treningId) {
        String sql = "SELECT * FROM terminTreninga WHERE treningId=? AND datum >= NOW()";
        return jdbcTemplate.query(sql, new TerminTreningaDAOImpl.TerminTreningaRowMapper(), treningId);
    }

    @Override
    public void rezervacija(int terminTreningaId, String korisnickoIme) {
        String sql = "INSERT INTO terminTreninga_korisnik (terminTreningaId, korisnickoIme) VALUES (?, ?)";
        jdbcTemplate.update(sql, terminTreningaId, korisnickoIme);
    }

    @Override
    public List<Izvestaj> getIzvestaji(LocalDateTime pocetniDatum, LocalDateTime krajnjiDatum) {
        String sql = "SELECT trening.trening.id, count(trening.terminTreninga.id) as brojZakazanihTreninga, count(trening.terminTreninga.id)*sum(trening.trening.cena) as ukupnaCena FROM trening.termintreninga inner join trening.rezervacija inner join trening.trening on termintreninga.id = rezervacija.terminTreningaId and trening.id = termintreninga.treningId WHERE datumVreme >= ? AND datumVreme <= ? GROUP BY trening.trening.id;";
        return jdbcTemplate.query(sql, new IzvestajRowMapper(), pocetniDatum, krajnjiDatum);
    }

    @Override
    public TerminTreninga findTerminTreningaBySalaAndDatum(Sala sala, LocalDateTime pocetniDatum, LocalDateTime krajnjiDatum){
        try {
            String sql = "SELECT termintreninga.*, addtime(datum, trening.trajanje*100) as krajnjiDatum FROM trening.termintreninga INNER JOIN trening ON termintreninga.treningId=trening.id having salaId = ? and (datum between ? and ?" +
                    "or ? between datum and krajnjiDatum);";
            return jdbcTemplate.queryForObject(sql, new TerminTreningaRowMapper(), sala.getId(), pocetniDatum, krajnjiDatum, pocetniDatum);
        }
        catch(EmptyResultDataAccessException exc){
            return null;
        }

    }


}
