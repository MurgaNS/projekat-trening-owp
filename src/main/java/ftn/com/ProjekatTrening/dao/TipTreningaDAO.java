package ftn.com.ProjekatTrening.dao;

import ftn.com.ProjekatTrening.model.TipTreninga;

import java.util.List;

public interface TipTreningaDAO {
    public TipTreninga findOne(long id);

    public List<TipTreninga> findAll();

    /*
    public List<TipTreninga> find(String korisnickoIme, String eMail, String pol, Boolean administrator);

     */

    public void save(TipTreninga tipTreninga);

    public void update(TipTreninga tipTreninga);

    public void delete(long id);
}
