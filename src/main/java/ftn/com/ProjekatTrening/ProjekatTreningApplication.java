package ftn.com.ProjekatTrening;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjekatTreningApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjekatTreningApplication.class, args);
	}

}
