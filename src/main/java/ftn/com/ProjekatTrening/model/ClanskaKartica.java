package ftn.com.ProjekatTrening.model;

public class ClanskaKartica {
    private long id;
    private int popust;
    private int brojPoena;
    private EStatusClanskeKartice status;
    private Korisnik korisnik;

    public ClanskaKartica(long id, int popust, int brojPoena, EStatusClanskeKartice status, Korisnik korisnik) {
        this.id = id;
        this.popust = popust;
        this.brojPoena = brojPoena;
        this.status = status;
        this.korisnik = korisnik;
    }

    public ClanskaKartica(int popust, int brojPoena, EStatusClanskeKartice status, Korisnik korisnik) {
        this.popust = popust;
        this.brojPoena = brojPoena;
        this.status = status;
        this.korisnik = korisnik;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPopust() {
        return popust;
    }

    public void setPopust(int popust) {
        this.popust = popust;
    }

    public int getBrojPoena() {
        return brojPoena;
    }

    public void setBrojPoena(int brojPoena) {
        this.brojPoena = brojPoena;
    }

    public EStatusClanskeKartice getStatus() {
        return status;
    }

    public void setStatus(EStatusClanskeKartice status) {
        this.status = status;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }
}
