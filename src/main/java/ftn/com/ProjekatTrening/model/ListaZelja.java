package ftn.com.ProjekatTrening.model;

public class ListaZelja {
    private long id;
    private Trening trening;
    private Korisnik korisnik;

    public ListaZelja(long id, Trening trening, Korisnik korisnik) {
        this.id = id;
        this.trening = trening;
        this.korisnik = korisnik;
    }

    public ListaZelja(Trening trening, Korisnik korisnik) {
        this.trening = trening;
        this.korisnik = korisnik;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Trening getTrening() {
        return trening;
    }

    public void setTrening(Trening trening) {
        this.trening = trening;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }
}
