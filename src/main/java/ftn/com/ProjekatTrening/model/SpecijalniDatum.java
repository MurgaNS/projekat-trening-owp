package ftn.com.ProjekatTrening.model;

import java.time.LocalDate;

public class SpecijalniDatum {

    private long id;
    private LocalDate datum;
    private int popust;
    private Trening trening;

    public SpecijalniDatum(long id, LocalDate datum, int popust, Trening trening) {
        this.id = id;
        this.datum = datum;
        this.popust = popust;
        this.trening = trening;
    }

    public SpecijalniDatum(LocalDate datum, int popust, Trening trening) {
        this.datum = datum;
        this.popust = popust;
        this.trening = trening;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public int getPopust() {
        return popust;
    }

    public void setPopust(int popust) {
        this.popust = popust;
    }

    public Trening getTrening() {
        return trening;
    }

    public void setTrening(Trening trening) {
        this.trening = trening;
    }
}
