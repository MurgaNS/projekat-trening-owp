package ftn.com.ProjekatTrening.model;

public class TipTreninga {

    private long id;
    private String ime;
    private String opis;

    public TipTreninga(long id, String ime, String opis) {
        this(ime, opis);
        this.id = id;
    }

    public TipTreninga(String ime, String opis) {
        this.ime = ime;
        this.opis = opis;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
}
