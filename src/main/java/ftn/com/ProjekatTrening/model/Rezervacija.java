package ftn.com.ProjekatTrening.model;

import java.time.LocalDateTime;

public class Rezervacija {

    private long id;
    private TerminTreninga terminTreninga;
    private Korisnik korisnik;
    private LocalDateTime datumVreme;
    private double cena;

    public Rezervacija(long id, TerminTreninga terminTreninga, Korisnik korisnik, double cena) {
        this.id = id;
        this.terminTreninga = terminTreninga;
        this.korisnik = korisnik;
        this.cena = cena;
    }

    public Rezervacija(TerminTreninga terminTreninga, Korisnik korisnik, double cena) {
        this.terminTreninga = terminTreninga;
        this.korisnik = korisnik;
        this.cena = cena;
    }

    public Rezervacija(long id, TerminTreninga terminTreninga, Korisnik korisnik, LocalDateTime datumVreme, double cena) {
        this.id = id;
        this.terminTreninga = terminTreninga;
        this.korisnik = korisnik;
        this.datumVreme = datumVreme;
        this.cena = cena;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TerminTreninga getTerminTreninga() {
        return terminTreninga;
    }

    public void setTerminTreninga(TerminTreninga terminTreninga) {
        this.terminTreninga = terminTreninga;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public LocalDateTime getDatumVreme() {
        return datumVreme;
    }

    public void setDatumVreme(LocalDateTime datumVreme) {
        this.datumVreme = datumVreme;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }
}
