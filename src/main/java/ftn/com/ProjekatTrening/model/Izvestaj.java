package ftn.com.ProjekatTrening.model;

public class Izvestaj {
    private Trening trening;
    private int brojZakazanihTreninga;
    private double ukupnaCena;

    public Izvestaj(Trening trening, int brojZakazanihTreninga, double ukupnaCena) {
        this.trening = trening;
        this.brojZakazanihTreninga = brojZakazanihTreninga;
        this.ukupnaCena = ukupnaCena;
    }


    public Trening getTrening() {
        return trening;
    }

    public void setTrening(Trening trening) {
        this.trening = trening;
    }

    public int getBrojZakazanihTreninga() {
        return brojZakazanihTreninga;
    }

    public void setBrojZakazanihTreninga(int brojZakazanihTreninga) {
        this.brojZakazanihTreninga = brojZakazanihTreninga;
    }

    public double getUkupnaCena() {
        return ukupnaCena;
    }

    public void setUkupnaCena(double ukupnaCena) {
        this.ukupnaCena = ukupnaCena;
    }
}
