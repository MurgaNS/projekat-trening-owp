package ftn.com.ProjekatTrening.model;

public class Trening {

    private long id;
    private String naziv;
    private String treneri;
    private String opis;
    private String slika;
    private TipTreninga tip;
    private double cena;
    private EVrstaTreninga vrsta;
    private ENivoTreninga nivo;
    private int trajanje;
    private double prosecnaOcena;

    public Trening(long id, String naziv, String treneri, String opis, String slika, TipTreninga tip, double cena, EVrstaTreninga vrsta, ENivoTreninga nivo, int trajanje) {
        this.id = id;
        this.naziv = naziv;
        this.treneri = treneri;
        this.opis = opis;
        this.slika = slika;
        this.tip = tip;
        this.cena = cena;
        this.vrsta = vrsta;
        this.nivo = nivo;
        this.trajanje = trajanje;
    }

    public Trening(String naziv, String treneri, String opis, String slika, TipTreninga tip, double cena, EVrstaTreninga vrsta, ENivoTreninga nivo, int trajanje) {
        this.naziv = naziv;
        this.treneri = treneri;
        this.opis = opis;
        this.slika = slika;
        this.tip = tip;
        this.cena = cena;
        this.vrsta = vrsta;
        this.nivo = nivo;
        this.trajanje = trajanje;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getTreneri() {
        return treneri;
    }

    public void setTreneri(String treneri) {
        this.treneri = treneri;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public TipTreninga getTip() {
        return tip;
    }

    public void setTip(TipTreninga tip) {
        this.tip = tip;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public EVrstaTreninga getVrsta() {
        return vrsta;
    }

    public void setVrsta(EVrstaTreninga vrsta) {
        this.vrsta = vrsta;
    }

    public ENivoTreninga getNivo() {
        return nivo;
    }

    public void setNivo(ENivoTreninga nivo) {
        this.nivo = nivo;
    }

    public int getTrajanje() {
        return trajanje;
    }

    public void setTrajanje(int trajanje) {
        this.trajanje = trajanje;
    }

    public double getProsecnaOcena() {
        return prosecnaOcena;
    }

    public void setProsecnaOcena(double prosecnaOcena) {
        this.prosecnaOcena = prosecnaOcena;
    }
}
