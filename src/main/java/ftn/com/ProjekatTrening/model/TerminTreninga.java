package ftn.com.ProjekatTrening.model;

import java.time.LocalDateTime;

public class TerminTreninga {
    private long id;
    private Sala sala;
    private Trening trening;
    private LocalDateTime datum;

    public TerminTreninga(long id, Sala sala, Trening trening, LocalDateTime datum) {
        this.id = id;
        this.sala = sala;
        this.trening = trening;
        this.datum = datum;
    }

    public TerminTreninga(Sala sala, Trening trening, LocalDateTime datum) {
        this.sala = sala;
        this.trening = trening;
        this.datum = datum;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public Trening getTrening() {
        return trening;
    }

    public void setTrening(Trening trening) {
        this.trening = trening;
    }

    public LocalDateTime getDatum() {
        return datum;
    }

    public void setDatum(LocalDateTime datum) {
        this.datum = datum;
    }
}
