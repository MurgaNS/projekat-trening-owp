package ftn.com.ProjekatTrening.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Korisnik {
    private String korisnickoIme;
    private String lozinka;
    private String email;
    private String ime;
    private String prezime;
    private LocalDate datumRodjenja;
    private String adresa;
    private String brojTelefona;
    private LocalDateTime datumVremeRegistracije;
    private EUloga uloga;
    private boolean blokiran;



    public Korisnik(String korisnickoIme, String lozinka, String email, String ime, String prezime, LocalDate datumRodjenja, String adresa, String brojTelefona, LocalDateTime datumVremeRegistracije, EUloga uloga, boolean blokiran) {
        this.korisnickoIme = korisnickoIme;
        this.lozinka = lozinka;
        this.email = email;
        this.ime = ime;
        this.prezime = prezime;
        this.datumRodjenja = datumRodjenja;
        this.adresa = adresa;
        this.brojTelefona = brojTelefona;
        this.datumVremeRegistracije = datumVremeRegistracije;
        this.uloga = uloga;
        this.blokiran = blokiran;
    }

    public Korisnik(String korisnickoIme, String lozinka, String email, String ime, String prezime, LocalDate datumRodjenja, String adresa, String brojTelefona, EUloga uloga, boolean blokiran) {
        this.korisnickoIme = korisnickoIme;
        this.lozinka = lozinka;
        this.email = email;
        this.ime = ime;
        this.prezime = prezime;
        this.datumRodjenja = datumRodjenja;
        this.adresa = adresa;
        this.brojTelefona = brojTelefona;
        this.uloga = uloga;
        this.blokiran = blokiran;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public LocalDate getDatumRodjenja() {
        return datumRodjenja;
    }

    public void setDatumRodjenja(LocalDate datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getBrojTelefona() {
        return brojTelefona;
    }

    public void setBrojTelefona(String brojTelefona) {
        this.brojTelefona = brojTelefona;
    }

    public LocalDateTime getDatumVremeRegistracije() {
        return datumVremeRegistracije;
    }

    public void setDatumVremeRegistracije(LocalDateTime datumVremeRegistracije) {
        this.datumVremeRegistracije = datumVremeRegistracije;
    }

    public EUloga getUloga() {
        return uloga;
    }

    public void setUloga(EUloga uloga) {
        this.uloga = uloga;
    }

    public boolean isBlokiran() {
        return blokiran;
    }

    public void setBlokiran(boolean blokiran) {
        this.blokiran = blokiran;
    }

    public boolean getAdministrator(){
        return uloga == EUloga.ADMINISTRATOR;
    }

    public boolean isAdministrator(){
        return uloga == EUloga.ADMINISTRATOR;
    }

    public boolean getClan(){
        return uloga == EUloga.CLAN;
    }

    public boolean isClan(){
        return uloga == EUloga.CLAN;
    }
}
