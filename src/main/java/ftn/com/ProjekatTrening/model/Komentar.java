package ftn.com.ProjekatTrening.model;

import java.time.LocalDate;

public class Komentar {

    private long id;
    private String tekst;
    private int ocena;
    private LocalDate datum;
    private Korisnik korisnik;

    private Trening trening;

    private EStatusKomentara status;
    private boolean anoniman;
    public Komentar(){

    }
    public Komentar(long id, String tekst, int ocena, LocalDate datum, Korisnik korisnik, Trening trening, EStatusKomentara status, boolean anoniman) {
        this.id = id;
        this.tekst = tekst;
        this.ocena = ocena;
        this.datum = datum;
        this.korisnik = korisnik;
        this.trening = trening;
        this.status = status;
        this.anoniman = anoniman;
    }

    public Komentar(String tekst, int ocena, LocalDate datum, Korisnik korisnik, Trening trening, EStatusKomentara status, boolean anoniman) {
        this.tekst = tekst;
        this.ocena = ocena;
        this.datum = datum;
        this.korisnik = korisnik;
        this.trening = trening;
        this.status = status;
        this.anoniman = anoniman;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public int getOcena() {
        return ocena;
    }

    public void setOcena(int ocena) {
        this.ocena = ocena;
    }

    public LocalDate getDatum() {
        return datum;
    }

    public void setDatum(LocalDate datum) {
        this.datum = datum;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Trening getTrening() {
        return trening;
    }

    public void setTrening(Trening trening) {
        this.trening = trening;
    }

    public EStatusKomentara getStatus() {
        return status;
    }

    public void setStatus(EStatusKomentara status) {
        this.status = status;
    }

    public boolean isAnoniman() {
        return anoniman;
    }

    public void setAnoniman(boolean anoniman) {
        this.anoniman = anoniman;
    }
}
