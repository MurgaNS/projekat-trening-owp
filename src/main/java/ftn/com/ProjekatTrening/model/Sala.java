package ftn.com.ProjekatTrening.model;

public class Sala {

    private long id;
    private String oznaka;
    private int kapacitet;

    public Sala(long id, String oznaka, int kapacitet) {
        this.id = id;
        this.oznaka = oznaka;
        this.kapacitet = kapacitet;
    }

    public Sala(String oznaka, int kapacitet) {
        this.oznaka = oznaka;
        this.kapacitet = kapacitet;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOznaka() {
        return oznaka;
    }

    public void setOznaka(String oznaka) {
        this.oznaka = oznaka;
    }

    public int getKapacitet() {
        return kapacitet;
    }

    public void setKapacitet(int kapacitet) {
        this.kapacitet = kapacitet;
    }
}
