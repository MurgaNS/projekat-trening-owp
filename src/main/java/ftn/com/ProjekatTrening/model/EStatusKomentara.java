package ftn.com.ProjekatTrening.model;

public enum EStatusKomentara {
    NA_CEKANJU, ODOBREN, NIJE_ODOBREN
}
